-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Sty 2018, 01:35
-- Wersja serwera: 10.1.26-MariaDB
-- Wersja PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `zstie`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `confirmKey` text NOT NULL,
  `ssid` text NOT NULL,
  `confirmed` text NOT NULL,
  `active` text NOT NULL,
  `gender` text NOT NULL,
  `registerDate` text NOT NULL,
  `lastLoginDate` text NOT NULL,
  `canonicalRegisterDate` text NOT NULL,
  `canonicalLastLoginDate` text NOT NULL,
  `canonicalLastActionDate` text NOT NULL,
  `uuid` text NOT NULL,
  `role` text NOT NULL,
  `lastActionDate` text NOT NULL,
  `passwordResetCode` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `confirmKey`, `ssid`, `confirmed`, `active`, `gender`, `registerDate`, `lastLoginDate`, `canonicalRegisterDate`, `canonicalLastLoginDate`, `canonicalLastActionDate`, `uuid`, `role`, `lastActionDate`, `passwordResetCode`) VALUES
(1, 'kromacie@gmail.com', '7abca48cf1f32f2c9746815bff7e0b59', 'b78b19e42602aaa75e2cbf49401949b5', 'o469mephcjklf2u2t0irjoi7g4', '1', '1', 'Mezczyzna', '2018-01-04 00:30:30', '2018-01-04 00:59:57', '1515022230', '', '1515023997', '', 'ROLE_USER', '', '');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
