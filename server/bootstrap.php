<?php
//Wczytanie z ręki potrzebnych plików - do przerobienia
require_once('vendor/autoload.php');
require_once('Managment/QueryBuilder/autoload.php');

require_once('Managment/Session.php');
require_once('Managment/AccessControl.php');

require_once('Managment/Collection/Collection.php');
require_once('Managment/Collection/UserCollection.php');

require_once('Managment/Factory/Factory.php');
require_once('Managment/Factory/UserFactory.php');

require_once('Managment/Validator/FormValidator.php');
require_once('Managment/Notificationer/Notificationer.php');
require_once('Managment/CSRFCode.php');

require_once('Router.php');
require_once('Managment/Route.php');
require_once('Managment/View.php');

require_once('Controller/Controller.php');
require_once('Controller/DefaultController.php');

require_once('Managment/User/UserBase.php');
require_once('Managment/User/Anonymous.php');
require_once('Managment/User/Admin.php');
require_once('Managment/User/User.php');



require_once('Managment/Transaction.php');
require_once('Managment/PDODatabase.php');
require_once('Managment/Mailer.php');
require_once('Managment/Date.php');





