<?php

class DefaultController extends Controller {
    function indexAction() {
        $router = new Router();
        $notify = Notificationer::show($router->getUri());

        return $this->render(new View("site/main.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "aside" => "backsite/templates/aside.php",
            "head" => "backsite/templates/head_main.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "notify" => $notify

        ]));

    }

    function loginAction() {

        $router = new Router();
        $gen = new CSRFCode();
        $notify = Notificationer::show($router->getUri());

        return $this->render(new View("site/subsites/zaloguj.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "path" => LOCAL."login/process",
            "token" => $gen->generate(),
            "notify" => $notify
        ]));

    }

    function loginProcessAction() {
        $token = isset($_POST['token']) ? $_POST['token'] : false;
        $login = isset($_POST['login']) ? $_POST['login'] : false;
        $password = isset($_POST['password']) ? $_POST['password'] : false;
        $gen = new CSRFCode();
        $router = new Router();
        if(!$gen->isValid($token)){
            Notificationer::set("Error", false, "/zaloguj", 1);
            $router->createRoute(LOCAL."zaloguj");
            $gen->delete();
            return false;
        }
        $gen->delete();

        $validator = new FormValidator();
        $validator->valid(new LoginType(), $login)
                  ->valid(new PasswordType(), $password);

        if(!$validator->processIsValid()){
            Notificationer::set("Niepoprawny login lub hasło!", false, "/zaloguj", 1);
            $router->createRoute(LOCAL."zaloguj");
            return false;
        }

        $user = new User();
        $user->select()
                ->column("role")
                ->column("id")
             ->where()
                ->column("password")->is(md5($password))->and()
                ->column("login")->is($login);

        $user->get();

        if($user->getRole() === null){
            Notificationer::set("Niepoprawny login lub hasło!", false, "/zaloguj", 1);
            $router->createRoute(LOCAL."zaloguj");
            return false;
        }

        $ssid = session_id();
        $date = new Date();
        $current = $date->getCurrent();
        $user->update()
                ->column("lastLoginDate")->set($current)
                ->column("canonicalLastLoginDate")->set($date->toSeconds($current))
                ->column("ssid")->set($ssid)
             ->where()
                ->column("id")->is($user->getId());

        $user->get();

        Session::set("id", $user->getId());
        Session::set("role", $user->getRole());

        Notificationer::set("Zalogowano pomyślnie!", true, "/", 1);
        $router->createRoute(LOCAL);
        return true;
    }

    function registerAction() {
        $router = new Router();
        $gen = new CSRFCode();
        $notify = Notificationer::show($router->getUri());

        return $this->render(new View("site/subsites/zarejestruj.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "aside" => "backsite/templates/aside.php",
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "path" => LOCAL."register/process",
            "token" => $gen->generate(),
            "notify" => $notify
        ]));
    }

    function registerProcessAction() {
        $login = isset($_POST['user_email']) ? $_POST['user_email'] : false;
        $password1 = isset($_POST['user_password1']) ? $_POST['user_password1'] : false;
        $password2 = isset($_POST['user_password2']) ? $_POST['user_password2'] : false;
        $gender = isset($_POST['user_gender']) ? $_POST['user_gender'] : false;
        $gender = $gender == 1 ? "Mezczyzna" : "Kobieta";
        $token = isset($_POST['token']) ? $_POST['token'] : false;
        $reg = isset($_POST["website_privacy"]) ? $_POST["website_privacy"] : false;

        $router = new Router();
        $gen = new CSRFCode();
        $date = new Date();

        if(!$gen->isValid($token)){
            $gen->delete();
            $router->createRoute(LOCAL."zarejestruj");
            return false;
        }
        $gen->delete();

        if(!$reg){
            Notificationer::set("Zaakceptuj regulamin!", false, "/zarejestruj", 1);
            $router->createRoute(LOCAL."zarejestruj");
        }
        $validator = new FormValidator();
        $validator->valid(new PasswordType(), $password1)
                  ->valid(new LoginType(), $login);

        if(!$validator->processIsValid()){
            Notificationer::set($validator->getError(), false, "/zarejestruj", 1);
            $router->createRoute(LOCAL."zarejestruj");
            return false;
        }

        if($password1 !== $password2){
            Notificationer::set("Hasła nie są takie same", false, "/zarejestruj", 1);
            $router->createRoute(LOCAL."zarejestruj");
            return false;
        }

        $user = new User();
        $user->select()
                ->column("role")
             ->where()
                ->column("login")->is($password1);

        $user->get();

        if($user->getRole() != null){
            Notificationer::set("Konto o podanym mailu już istnieje!", false, "/zarejestruj", 1);
            $router->createRoute(LOCAL."zarejestruj");
            return false;
        }

        $current = $date->getCurrent();
        $canonical = $date->toSeconds($current);
        $key = md5(microtime().rand());

        $mailer = new Mailer();
        $mailer->addContext("Potwierdz rejestracje 1", "http://".$_SERVER["HTTP_HOST"].LOCAL."register/confirm/".$key);
        $mailer->addTarget($login, "Użytkowniku!");
        $mailer->send();

        $user->insert()
               ->column("login")->set($login)
               ->column("password")->set(md5($password1))
               ->column("registerDate")->set($current)
               ->column("active")->set(1)
               ->column("confirmed")->set(0)
               ->column("role")->set("ROLE_USER")
               ->column("gender")->set($gender)
               ->column("confirmKey")->set($key);

        $user->get();



        Notificationer::set("Kod potwierdzający został wysłany na email!", true, "/zarejestruj", 1);
        $router->createRoute(LOCAL."zarejestruj");
        return true;

    }

    function registerConfirmAction($data) {
        $router = new Router();
        if(!isset($data['key'])){
            // $router->createRoute(LOCAL."zaloguj");
            return false;
        }

        $key = $data['key'];

        $validator = new FormValidator();
        $validator->valid(new ConfirmKeyType(), $key);
        if(!$validator->processIsValid()){
            Notificationer::set("Kod jest niepoprawny!", false, "/zaloguj", 1);
            $router->createRoute(LOCAL."zaloguj");
            return false;
        }

        $user = new User();
        $user->select()
                ->column("*")
             ->where()
                ->column("confirmKey")->is($key)->and()
                ->column("confirmed")->is(0);

        $user->get();

        if($user->getRole() == null) {
            Notificationer::set("Kod jest niepoprawny lub został już użyty!", false, "/zaloguj", 1);
            $router->createRoute(LOCAL."zaloguj");
            return false;
        }

        $id = $user->getId();

        $user->update()
                ->column("confirmed")->set(1)
             ->where()
                ->column("id")->is($id);

        $user->get();

        Notificationer::set("Możesz się zalogować!", true, "/zaloguj", 1);
        $router->createRoute(LOCAL."zaloguj");
        return true;

    }

    function profileAction() {
        $router = new Router();
        $id = Session::get('id');
        $user = new User();
        $user->select()
                ->column("login")
              ->where()
                ->column("id")->is($id);

        $user->get();
        $notify = Notificationer::show($router->getUri());
        return $this->render(new View("site/subsites/profil.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "aside" => "backsite/templates/aside.php",
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "path" => LOCAL."register/process",
            "rank" => "Śmieszek",
            "name" => $user->getLogin(),
            "articles" => 0,
            "notify" => $notify
        ]));
    }

    function newsAction() {
        return $this->render(new View("site/subsites/nowosci.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "aside" => "backsite/templates/aside.php",
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php"
        ]));
    }

    function logoutAction() {
        $router = new Router();
        Session::delete(['id', 'role']);
        session_regenerate_id();
        Notificationer::set("Wylogowano!", true, "/", 1);
        $router->createRoute(LOCAL);
        return true;
    }

    function changeEmailAction()
    {
        $gen = new CSRFCode();
        $token = $gen->generate();
        $router = new Router();
        $notify = Notificationer::show($router->getUri());

        return $this->render(new View("site/subsites/nowy_email.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "aside" => "backsite/templates/aside.php",
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "path" => LOCAL."profil/email/process",
            "token" => $token,
            "notify" => $notify
        ]));
    }

    function changePasswordAction()
    {
        $gen = new CSRFCode();
        $token = $gen->generate();
        $router = new Router();
        $notify = Notificationer::show($router->getUri());

        return $this->render(new View("site/subsites/nowe_haslo.php", [
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "aside" => "backsite/templates/aside.php",
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "path" => LOCAL."profil/password/process",
            "token" => $token,
            "notify" => $notify
        ]));
    }

    function changeEmailProcessAction()
    {
        $router = new Router();
        $gen = new CSRFCode();
        $email = isset($_POST['new_email']) ? $_POST['new_email'] : false;
        $token = isset($_POST['token']) ? $_POST['token'] : false;

        if(!$email)
        {
            $router->createRoute(LOCAL."profil/zmien_email");
            return false;
        }

        if(!$gen->isValid($token))
        {
            $gen->delete();
            return false;
        }
        $gen->delete();

        $validator = new FormValidator();
        $validator->valid(new LoginType(), $email);
        if(!$validator->processIsValid())
        {
            Notificationer::set($validator->getError(), false, "/profil/zmien_email", 1);
            $router->createRoute(LOCAL."profil/zmien_email");
            return false;
        }

        $id = Session::get('id');
        $user = new User();
        $user->update()
                ->column("login")->set($email)
              ->where()
                ->column("id")->is($id);

        $user->get();

        Notificationer::set("Zmieniono Email!", true, "/profil", 1);
        $router->createRoute(LOCAL."profil");
        return true;
    }

    function changePasswordProcessAction()
    {
        $router = new Router();
        $gen = new CSRFCode();
        $pass1 = isset($_POST['new_password1']) ? $_POST['new_password1'] : false;
        $pass2 = isset($_POST['new_password2']) ? $_POST['new_password2'] : false;
        $token = isset($_POST['token']) ? $_POST['token'] : false;

        if(!$pass1 || !$pass2)
        {
            Notificationer::set("Podaj oba hasła!", false, "/profil/zmien_haslo", 1);
            $router->createRoute(LOCAL."profil/zmien_haslo");
            return false;
        }

        if(!$gen->isValid($token))
        {
            $router->createRoute(LOCAL."profil/zmien_haslo");
            $gen->delete();
            return false;
        }
        $gen->delete();

        if($pass1 != $pass2)
        {
            Notificationer::set("Hasła nie są takie same!", false, "/profil/zmien_haslo", 1);
            $router->createRoute(LOCAL."profil/zmien_haslo");
            return false;
        }

        $validator = new FormValidator();
        $validator->valid(new PasswordType(), $pass1);
        if(!$validator->processIsValid())
        {
            Notificationer::set($validator->getError(), false, "/profil/zmien_haslo", 1);
            $router->createRoute(LOCAL."profil/zmien_haslo");
            return false;
        }

        $id = Session::get('id');
        $user = new User();
        $user->update()
            ->column("password")->set(md5($pass1))
            ->where()
            ->column("id")->is($id);

        $user->get();

        Notificationer::set("Zmieniono hasło!", true, "/profil", 1);
        $router->createRoute(LOCAL."profil");
    }

    function resetPasswordAction()
    {
        $gen = new CSRFCode();
        $router = new Router();
        $notify = Notificationer::show($router->getUri());
        return $this->render(new View('site/subsites/zresetuj_haslo.php', [
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "path" => LOCAL."reset_password/send",
            "token" => $gen->generate(),
            'notify' => $notify
        ]));
    }

    function resetPasswordSendAction()
    {
        $token = isset($_POST['token']) ? $_POST['token'] : false;
        $gen = new CSRFCode();
        $router = new Router();

        if(!$gen->isValid($token))
        {
            $router->createRoute(LOCAL."zresetuj_haslo");
            $gen->delete();
            return false;
        }
        $gen->delete();

        $email = isset($_POST['user_email']) ? $_POST['user_email'] : false;

        $validator = new FormValidator();
        $validator->valid(new LoginType(), $email);
        if(!$validator->processIsValid())
        {
            Notificationer::set("Nie znaleziono użytkownika", false, "/zresetuj_haslo", 1);
            $router->createRoute(LOCAL."zresetuj_haslo");
            return false;
        }

        $user = new User();
        $user->select()
                ->column('*')
             ->where()
                ->column('login')->is($email);

        $user->get();

        if($user->getRole() != 'ROLE_USER')
        {
            Notificationer::set("Nie znaleziono użytkownika", false, "/zresetuj_haslo", 1);
            $router->createRoute(LOCAL."zresetuj_haslo");
            return false;            
        }  
        $key = md5(microtime().rand());
        $user->update()
                ->column('passwordResetCode')->set($key)
             ->where()
                ->column('id')->is($user->getId());

        $user->get();

        Notificationer::set("Wysłano kod potwierdzający na podany email", true, "/zresetuj_haslo", 1);

        $mailer = new Mailer();
        $mailer->addContext("Potwierdz zmiane hasła!", "http://".$_SERVER["HTTP_HOST"].LOCAL."nowe_haslo/".$key);
        $mailer->addTarget($user->getLogin(), "Użytkowniku!");
        $mailer->send();

        $router->createRoute(LOCAL."zresetuj_haslo");
        return false;   
    }

    function newsAddAction()
    {
        return $this->render(new View('site/subsites/dodaj_wiadomosc.php', [
            "head" => "backsite/templates/head_second.php",
            "header" => "backsite/templates/header.php",
            "nav" => "backsite/templates/nav.php",
            "footer" => "backsite/templates/footer.php",
            "style" => LOCAL."client/style/",
            "view" => LOCAL."client/view/",
            "client" => LOCAL,
            "path" => LOCAL."reset_password/send"            
        ]));
    }
}