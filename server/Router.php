<?php
class Router {
	/**
	 * 
	 */
	protected $trases;
	protected $lastCorrectRoute;
	protected $selectedRoute;

	public function load() {

		$config = $this->getConfig();
		
		foreach ($config as $path => $con) {

			$action = explode('::', $con);
			$uri    = $this->getRealUri($path);
			$args   = $this->getArgsPos($path);
			$route  = new Route($uri, $action);
			$route  ->applyArgsPos($args);
			$this   ->trases[$uri] = $route;						
		}

		return $this;
	}

	public function getRealUri($path)
	{
		$parts = explode('/{', $path);
		return $parts[0];
	}

	public function getArgsPos($path)
	{
		$args = [];
		$parts = explode('/', $path);
		foreach ($parts as $key => $part) {
			if(strpos($part, '}') && strpos($part, '{') !== false)
			{
				$sub = str_replace('}', "", $part);
				$sub = str_replace('{', "", $sub);
				$args[$key] = $sub;
			}
		}

		

		return $args;
	}

	public function get($uri)
	{
		try {
			if(!isset($this->trases[$uri])) {
				throw new Exception('Nie znaleziono kontrolera dla '.$uri);
			}
			return $this->trases[$uri];
		} catch (Exception $e) {	
			return false;	
		}
	
	}

	public function route() {

		$uri = $this->getUri();
		$pathKeys = explode('/', $uri);
		$uri = "";
		$this->setLastCorrectRoute($this->get("/"));
        if(!isset($_SESSION['request']))
        {
            $_SESSION['request'] = [];
        }

        $_SESSION['request'][] = $_SERVER['REQUEST_URI'];
		foreach ($pathKeys as $key) {
			if($key == null)
				continue;

			$uri .= '/'.$key;
			$searchingRoute = $this->searchRoute($uri);
			if($searchingRoute instanceof Route)
			{
				$this->setLastCorrectRoute($searchingRoute);				
			}
						
		}

		if($this->lastCorrectRoute instanceof Route) {
			$this->lastCorrectRoute->getVariablesFromUri($this->getUri());
		}
		
		$this->selectRoute($this->getlastCorrectRoute());
		$this->executeHandlers();
		$this->returnResponse();
	}

	public function executeHandlers()
	{
		$access = new AccessControl();
		$access->handle($this);	
	}

    /**
     *
     */
    public function returnResponse()
	{
		echo $this->getSelectedRoute()->route();
	}

	public function getLastCorrectRoute()
	{
		return $this->lastCorrectRoute;
	}

	public function setLastCorrectRoute(Route $route)
	{
		$this->lastCorrectRoute = $route;
	}

    /**
     * @return Route
     */
    public function getSelectedRoute()
	{
		return $this->selectedRoute;
	}

	public function selectRoute(Route $route)
	{
		$this->selectedRoute = $route;
	}

	public function searchRoute($key)
	{
		if($key == null)
		{
			$key = '/';
		}

		$route = $this->get($key);
		if($route) {
			return $route;
		}
		foreach ($this->trases as $name => $route) {
			if(strpos($name, $key) !== false){
				return $route;
			}
		}
	}

	public function getConfig()
	{
		$json = file_get_contents($this->getPath().'/server/Config/routing.json');
		$config = json_decode($json);
		return $config;		
	}

	public function getPath() 
	{
		return MV;
	}

	public function getUri() {
		$uri =  substr($_SERVER['REQUEST_URI'], strlen(LOCAL));
		if(strlen($uri) < 1)
		{
			return "";
		}
		return "/".$uri;
	}

	public function getPostData() {
		$args = [];
		foreach ($_POST as $key => $value) {
			$args[$key] = $value;
		}
		return $args;
	}

	public function createRoute($uri) {
		header('Location: '.$uri);
	}

}