<?php

class Session {
	static function get($data) {
		if(is_array($data)){
			$_data = [];
			foreach ($data as $key) {
				$_data[$key] = self::getValidValue($key);
			}
			return $_data;
		}
		return self::getValidValue($data);
	}
	static function delete($data) {
		if(is_array($data)){
			$_data = [];
			foreach ($data as $key) {
				if(isset($_SESSION[$key])){
					unset($_SESSION[$key]);
				}
			}
			return;
		}	

		if(isset($_SESSION[$data])){
			unset($_SESSION[$data]);
		}	
	}
	static function set($key, $data)
	{
		$_SESSION[$key] = $data;
	}
	static function getValidValue($key) {
		if(isset($_SESSION[$key])){
			return $_SESSION[$key];
		}
		return null;
	}
}