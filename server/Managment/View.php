<?php

class View {
	public $args;
	public $file;
	public $path;
	public $local;

    /**
     * View constructor.
     * @param $file
     * @param array $args
     */
    function __construct($file, $args = []) {
		$this->apply($args);
		$this->searchFile($file);
	}

    /**
     * @param $args
     */
    function apply($args) {
		$this->args = $args;
	}

    /**
     * @param $file
     */
    function searchFile($file) {
        $router = new Router();
		$path = $router->getPath();
		$parts = explode('/', $file);
		$this->local = $file;
		$this->path = $path.'/client/';
		$this->file = $path.'/client/'.$file;
	}

    /**
     * @return Closure
     */
    function setLoader() {
		return function($file){
			$this->path = Router::getPath();	
			$this->local = $file;
			$this->file = $this->path.'/client/'.$file;
			foreach ($this->args as $__key => $__value) {
				if(intval($__key))
					continue;
				$$__key = $__value;
			}
			$__path = $this->path;
			$__file = $this->file;
			$__local = $this->local;
			$__uri = Router::getUri();

			$load = $this->setLoader();
			$is_granted = $this->setUserVerification();

			try {
				if(!@$m = require($this->file)){
					throw new Exception('Problem z załadowaniem pliku: '.$this->local);
				}
			} catch (Exception $e) {
				echo $e->getMessage();
			}			
		};
	}

    /**
     * @return Closure
     */
    function setUserVerification() {
        /**
         * @param $role
         * @return bool
         */
        return function($role) {
				$__role = Session::get('role');

				if($__role == $role){
					return true;
				} 
				return false;
			};
	}

    /**
     * @return string
     */
    function create() {
        $router = new Router();
		foreach ($this->args as $__key => $__value) {
			if(intval($__key))
				continue;
			$$__key = $__value;
		}
		$__path = $this->path;
		$__file = $this->file;
		$__local = $this->local;
		$__uri = $router->getUri();

		$load = $this->setloader();
		$is_granted = $this->setUserVerification();

		try {
			if(!@require($this->file)){
				throw new Exception('Problem z załadowaniem pliku: '.$this->local);
			}
			$buffer = ob_get_contents();
			if($buffer) {
                ob_clean();
            }

			return $buffer;
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}
}