<?php

class Transaction {
    private $db;
    function __construct()
    {
        $db = new PDODatabase();
        $this->db = $db->get();
        $this->db->beginTransaction();
    }
    function add($query) {
        $this->db->exec($query);
    }
    function execute() {
        $this->db->commit();
    }
}