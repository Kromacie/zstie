<?php

class CSRFCode {
	function get() {
		if(isset($_SESSION['__csrf'])) {
			return $_SESSION['__csrf'];
		}
	}
	function isValid($code) {
		$_code = $this->get();
		if($_code == $code) {
			return true;
		}
		return false;
	}
	function generate() {
		$token = md5(microtime().rand());
		$_SESSION['__csrf'] = $token;
		return $token;
	}
	function delete() {
		if(isset($_SESSION['__csrf'])) {
			unset($_SESSION['__csrf']);
		}
	}
}