<?php
require_once('Notification/Notification.php');

class Notificationer {
	static function set($content, $type, $path, $livingTime) {
		$notification = new Notification();
		$notification->setPath($path)
 					 ->setContent($content)
 					 ->setType($type)
 					 ->setLivingTime($livingTime);

 		$val = serialize($notification);
		Session::set( $path.'__notification', $val);
	}
	static function get($path) {

		$notification = 0;
		$data = Session::get( [ $path.'__notification' ] );
		if($data[ $path.'__notification']) {
			$notification = unserialize($data[ $path.'__notification']);
			$notification->bind();
			if($notification->getLivingTime() <= 0) {
				Session::delete([$path.'__notification']);
			}
			return $notification;
		}

	}

	static function show($path) {
		$notification = self::get($path);
		if($notification instanceof Notification)
		{
			return $notification->get();
		}
	}
}