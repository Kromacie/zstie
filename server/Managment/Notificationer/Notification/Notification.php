<?php

class Notification {

	protected $type;
	protected $content;
	protected $path;
	protected $livingTime;
	protected $successClass;
	protected $failureClass;
	protected $class;
	protected $semantic;
	protected $id;


	function __construct() {
		$router = new Router();
		$config = file_get_contents($router->getPath().'/server/Config/notification.json');
		$config = json_decode($config);
		$this->successClass = $config->{'success_class'};
		$this->failureClass = $config->{'failure_class'};
		$this->id = $config->{'id'};
		$this->semantic = $config->{'semantic'};
	}
	function getLivingTime() {
		return $this->livingTime;
	}
	function setType($type) {
		if($type) {
			$this->class = $this->successClass;
		} else {
			$this->class = $this->failureClass;
		}
		$this->type = $type;
		return $this;
	}
	function bind() {
		$this->livingTime--;
	}
	function setContent($content) {
		$this->content = $content;
		return $this;
	}
	function setPath($path) {
		$this->path = $path;
		return $this;
	}
	function setLivingTime($time) {
		$this->livingTime = $time;
		return $this;
	}
	function get() {
		$class = $this->class;
		$id = $this->id;
		$semantic = $this->semantic;
		$content = $this->content;
		require('template.php');
		$buffer = ob_get_contents();
		if($buffer) {
		    ob_clean();
        }
        return $buffer;
	}
}