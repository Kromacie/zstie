<?php

class UserCollection extends Collection {
	protected $users = [];

    /**
     * @param $item
     * @return $this
     */
    function add($item) {
		$this->users[$item->getId()] = $item;
		return $this;
	}

    /**
     * @return array
     */
    function getAll() {
		return $this->users;
	}


}