<?php

abstract class Collection {
    protected $items = [];
    abstract function add($item);
    abstract function getAll();
}