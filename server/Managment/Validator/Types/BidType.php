<?php

class BidType extends BaseType {

	function check($value) {
		$this->valid = true;
		if(!is_numeric($value)) {
			$this->valid = false;
			$this->errMsg = 'Niepoprawna wartość Bid';
			return false;
		}
		if($value < 0) {
			$this->valid = false;
			$this->errMsg = 'Niepoprawna wartość Bid';
			return false;
		}
	}
	
}