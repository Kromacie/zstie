<?php

class PriceType extends BaseType {

    function check($value)
    {
        $this->valid = true;
        $parts = explode('.', $value);

        if(!$this->checkStr($value)) {
            $this->valid = false;
            $this->errMsg = "Nieprawidłowe znaki w cenie!";
            return false;
        }

        if(count($parts) > 2) {
            $this->valid = false;
            $this->errMsg = "Zbyt dużo kropek w cenie";
            return false;
        }

        if(count($parts) > 1) {
            if(strlen($parts[1]) > 3 || strlen($parts[1]) < 1) {
                $this->valid = false;
                $this->errMsg = "Nieprawidłowa ceny wartość po kropce";
                return false;
            }
            if(strlen($parts[0]) < 1) {
                $this->valid = false;
                $this->errMsg = "Nieprawidłowa ceny wartość po kropce";
                return false;
            }
        }

    }
    function checkStr($value) {
        $val = str_split($value);
        $permittedChars = "0123456789.";
        foreach ($val as $char) {
            if(strpos($permittedChars, $char) === false)
            {
                return false;
            }
        }
        return true;
    }
}