<?php 

class PasswordType extends BaseType {

	function check($value) {
		$this->valid = true;

		if(strlen($value) < 8 || strlen($value) > 60) {
			$this->valid = false;
			$this->errMsg = "Zła długość hasła";
			return false;
		}
		if(!$this->containsNumber($value)) {
			$this->valid = false;
			$this->errMsg = "Hasło musi posiadać przynajmniej jedną cyfrę!";
			return false;
		}
		if(!$this->containsBigDigt($value)) {
			$this->valid = false;
			$this->errMsg = "Hasło musi posiadać przynajmniej jedną dużą literę!";
			return false;
		}

		if($this->containsFailDigt($value)) {
			$this->valid = false;
			$this->errMsg = "Niedozwolone znaki w haśle";
			return false;
		}
	}

	function containsNumber($value) {
		$chars = str_split($value);
		$permittedChars = "1234567890";
		foreach ($chars as $char) {
			if(strpos($permittedChars, $char) !== false)
			{
				return true;
			}
		}
		return false;
	}

	function containsBigDigt($value) {
		$chars = str_split($value);
		$permittedChars = "QWERTYUIOPLKJHGFDSAZXCVBNM";
		foreach ($chars as $char) {
			if(strpos($permittedChars, $char) !== false)
			{
				return true;
			}
		}
		return false;
	}

	function containsFailDigt($value) {
		$chars = str_split($value);
		$permittedChars = "<>='";
		foreach ($chars as $char) {
			if(strpos($permittedChars, $char) !== false)
			{
				return true;
			}
		}
		return false;		
	}

}