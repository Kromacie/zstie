<?php 

class LoginType extends BaseType {

	function check($value) {

		$this->valid = true;

		if(intval($value))
		{
			if(strlen($value) != 9){
				$this->errMsg = "Błędny numer telefonu";
				$this->valid = false;
			}
		} else {
			if(strlen($value) < 10 || strlen($value) > 264){
				$this->errMsg = "Błędna długość emaila";
				$this->valid = false;
				return false;
			}

			if(strpos($value, '@') === false){
				$this->errMsg = "Niepoprawny email 1";
				$this->valid = false;
				return false;
			}

			$parts = explode('@', $value);

			if(count($parts) > 2){
				$this->errMsg = "Niepoprawny email 2";
				$this->valid = false;
				return false;
			}

			if(strlen($parts[0]) > 64){
				$this->errMsg = "Zbyt długi email";
				$this->valid = false;
				return false;
			}

			if(!$this->checkPart($parts[0])){
				$this->errMsg = "Niepoprawne znaki w emailu";
				$this->valid = false;
				return false;
			}

			if(strpos($parts[1], '.') === false){
				$this->errMsg = "Niepoprawny email 3";
				$this->valid = false;
				return false;
			}

			$subs = explode('.', $parts[1]);

			if(strlen($subs[0]) < 1){
				$this->errMsg = "Niepoprawny email 4";
				$this->valid = false;
				return false;
			}

			if(!$this->checkPart($subs[0])){
				$this->errMsg = "Niepoprawny email 5";
				$this->valid = false;
				return false;
			}

			if(strlen($subs[1]) < 1){
				$this->errMsg = "Niepoprawny email 6";
				$this->valid = false;
				return false;
			}

			if(!$this->checkPart($subs[1])){
				$this->errMsg = "Niepoprawny email 7";
				$this->valid = false;	
				return false;			
			}


		}

	}

	function checkPart($val) {
		if(!$val){
			return false;
		}
		$val = str_split(strtolower($val));
		$permittedChars = "1234567890qwertyuiopasdfghjklzxcvbnm._-";
		foreach ($val as $char) {
			if(strpos($permittedChars, $char) === false)
			{
				return false;
			}
		}
		return true;
	}

}