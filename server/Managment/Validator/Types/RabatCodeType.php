<?php

class RabatCodeType extends BaseType {
	function check($value) {
		$this->valid = true;
		if(strlen($value) != 8) {
			$this->valid = false;
			$this->errMsg = "Niepoprawny kod promocyjny";
			return false;			
		}
		if(!$this->checkStr(strtolower($value))) {
			$this->valid = false;
			$this->errMsg = "Niepoprawny kod promocyjny";
			return false;
		}
	}
	function checkStr($value) {
		$val = str_split($value);
		$permittedChars = "1234567890qwertyuiopasdfghjklzxcvbnm";
		foreach ($val as $char) {
			if(strpos($permittedChars, $char) === false)
			{
				return false;
			}
		}
		return true;
	}
}