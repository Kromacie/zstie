<?php

class GroupNameType extends BaseType {

    function check($value)
    {
        $this->valid = true;
        if(empty($value)){
            $this->errMsg = "Nazwa nie może być pusta";
            $this->valid = false;
            return false;
        }
        if($this->containsFailDigt($value)){
            $this->errMsg = "Nieprawidłowe znaki w nazwie";
            $this->valid = false;
            return false;
        }

    }

    function containsFailDigt($value) {
        $chars = str_split($value);
        $permittedChars = "<>=%/\\'";
        foreach ($chars as $char) {
            if(strpos($permittedChars, $char) !== false)
            {
                return true;
            }
        }
        return false;
    }
}