<?php

abstract class BaseType {
	protected $valid = false;
	protected $errMsg;

	abstract function check($value);
	
	function isValid() {
		return $this->valid;
	}
	function getErr() {
		return $this->errMsg;
	}
}