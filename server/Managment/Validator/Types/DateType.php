<?php

class DateType extends BaseType {

    function check($value)
    {
        $this->valid = true;
        if(!$this->checkStr($value) || $this->checkDate($value)) {
            $this->errMsg = "Niepoprawny format daty!";
            $this->valid = false;
            return false;
        }
    }
    function checkStr($value) {
        $val = str_split($value);
        $permittedChars = "1234567890:-' '";
        foreach ($val as $char) {
            if(strpos($permittedChars, $char) === false)
            {
                return false;
            }
        }
        return true;
    }

    function checkDate($value) {
        return DateTime::createFromFormat("Y-m-d H:i:s", $value);
    }
}