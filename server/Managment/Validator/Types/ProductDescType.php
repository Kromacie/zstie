<?php

class ProductDescType extends BaseType {

    function check($value)
    {
        $this->valid = true;
        if(empty($value)) {
            $this->valid = false;
            $this->errMsg = "Opis nie może być pusty";
            return false;
        }
    }
}