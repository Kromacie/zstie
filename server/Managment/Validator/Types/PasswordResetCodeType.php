<?php

class PasswordResetCodeType extends BaseType {
	function check($value) {
		$this->valid = true;
		
		if(strlen($value) != 32 || !$this->checkStr($value)){
			$this->valid = false;
			$this->errMsg = "Podany kod resetowania jest niepoprawny!";
			return $this->valid;
		}
	}

	function checkStr($value) {
		$val = str_split($value);
		$permittedChars = "1234567890qwertyuiopasdfghjklzxcvbnm";
		foreach ($val as $char) {
			if(strpos($permittedChars, $char) === false)
			{
				return false;
			}
		}
		return true;
	}
}