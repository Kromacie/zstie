<?php

class HttpUrlType extends BaseType {

    function check($value)
    {
        $this->valid = true;
        if(empty($value)){
            $this->valid = false;
            $this->errMsg = "Obrazek jest wymagany!";
            return false;
        }
        if($this->containsFailDigt($value)){
            $this->valid = false;
            $this->errMsg = "Złe znaki w linku";
            return false;
        }
    }
    function containsFailDigt($value) {
        $chars = str_split($value);
        $permittedChars = "'<>";
        foreach ($chars as $char) {
            if(strpos($permittedChars, $char) !== false)
            {
                return true;
            }
        }
        return false;
    }
}