<?php
require_once('Types/BaseType.php');
require_once('Types/LoginType.php');
require_once('Types/TextType.php');
require_once('Types/PasswordType.php');
require_once('Types/ConfirmKeyType.php');
require_once('Types/RabatCodeType.php');
require_once('Types/AdminPasswordType.php');
require_once('Types/BidType.php');
require_once('Types/IdType.php');
require_once('Types/PasswordResetCodeType.php');
require_once('Types/GroupNameType.php');
require_once('Types/ProductDescType.php');
require_once('Types/PriceType.php');
require_once('Types/HttpUrlType.php');
require_once('Types/DateType.php');

class FormValidator {
	private $process;
	private $error;
	function __construct() {
		$this->process = true;
		$this->error;
	}
	function valid(BaseType $type, $context) {
		$type->check($context);
		if(!$type->isValid()){
			$this->error = $type->getErr();
			$this->process = false;
		}
		return $this;
	}

	function processIsValid() {
		return $this->process;
	}
	function getError() {
		return $this->error;
	}
}