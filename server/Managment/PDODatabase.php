<?php
use QueryBuilder\Engine\DBConfig;

class PDODatabase {
	private $host;
	private $login;
	private $database;
	private $__pdo;

	function __construct() {
		$router = new Router();
		$config = file_get_contents($router->getPath().'/server/Config/database.json');
		$config = json_decode($config);
		try {
			if(!isset($config->{'host'})){
				throw new Exception('PDO: Nie podano hosta');
			} else {
				$this->host = $config->{'host'};
			}
			if(!isset($config->{'login'})){
				throw new Exception("PDO: Nie podano loginu");
			} else {
				$this->login = $config->{'login'};				
			}
			if(!isset($config->{'database'})){
				throw new Exception("PDO: Nie wybrano bazy danych");
			} else {
				$this->database = $config->{'database'};
			}
			if(!isset($config->{'password'})){
				throw new Exception("PDO: Nie podano hasła");
			} else {
				$this->password = $config->{'password'};
			}
			try {
			    DBConfig::$PASSWORD = $this->password;
			    DBConfig::$LOGIN = $this->login;
			    DBConfig::$HOST = $this->host;
			    DBConfig::$DATABASE = $this->database;
			    DBConfig::runPDO();
			} catch (PDOException $e) {
			    echo 'Problem z połączeniem z bazą!';
			}

		} catch (Exception $e) {
			echo $e->getMessage();
			return false;
		}
	}
	function get() {
		return $this->__pdo;
	}
}