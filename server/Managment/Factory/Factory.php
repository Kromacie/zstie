<?php

abstract class Factory {

    private $table;

    function __construct($table)
    {
        $this->table = $table;
    }

    protected $id;
	abstract function create();
	abstract function get();
	abstract function update();
	abstract function delete();
	abstract function getAll();

	function setId($id, $type = "=") {
	    if($id){
            $this->id = $type."'$id'";
        }
		return $this;
	}

    /**
     * @return bool|string
     */
    public function buildSelectQuery() {
		$query = "SELECT * FROM $this->table WHERE ";
		$_query = "";
		foreach ($this as $key => $value) {
			if(!isset($this->$key) || $key == "table"){
                continue;
            }


			$_query .= $key.$value." AND ";
		}

		if($_query == "") {
			$_query = "1 = 1;";
		} else {
            $_query = substr($_query, 0, -5).";";

        }
		return $query.$_query;
	}
	public function buildDeleteQuery() {
        $query = "DELETE FROM $this->table WHERE ";
        foreach ($this as $key => $value) {
            if(!isset($this->$key) || $key == "table")
                continue;

            $query .= $key.$value." AND ";
        }
        $query = substr($query, 0, -5).";";
        if($query == "") {
            $query = "1 = 1";
        }
        return $query;
    }
	public function buildUpdateQuery() {
		$query = "UPDATE $this->table SET ";
		foreach ($this as $key => $value) {
            if(!isset($this->$key) || $key == "table")
                continue;

			$query .= $key.$value.",";
		}
		$query = substr($query, 0, -1)." WHERE id$this->id;";
		return $query;
	}

	function buildCreateQuery() {
        $__query = "INSERT INTO $this->table (";
		$query = [];
		$query['keys'] = "";
		$query['values'] = "";
		foreach ($this as $key => $value) {
            if(!isset($this->$key) || $key == "table")
                continue;

			$query['keys'] .= $key.',';
			$query['values'] .= substr_replace($value, '', 0, 1).",";

		}
		$query['keys'] = substr($query['keys'], 0, -1);
        $query['values'] = substr($query['values'], 0, -1);

        $__query .= $query['keys'].") VALUES (";
		$__query .= $query['values'].");";

		return $__query;
	}
}