<?php

class UserFactory extends Factory {

	protected $login;
	protected $password;
	protected $confirmKey;
	protected $ssid;
	protected $confirmed;
	protected $active;
	protected $registerDate;
	protected $lastLoginDate;
	protected $password_reset_code;
	protected $lastActionDate;
	protected $role;
	protected $uuid;
	protected $canonicalLastActionDate;
	protected $canonicalLastLoginDate;
	protected $canonicalRegisterDate;
	protected $gender;

	function __construct()
    {
        parent::__construct("users");
    }

    /**
     * @param $role
     * @return $this
     */
    function setRole($role, $type = "=") {
        if(is_null($role) === false)
		    $this->role = $type."'$role'";
		return $this;
	}

    /**
     * @param $date
     * @return $this
     */
    function setLastLoginDate($date, $type = "=") {
        if(is_null($date) === false)
		    $this->lastLoginDate = $type."'$date'";
		return $this;
	}

    /**
     * @param $date
     * @return $this
     */
    function setRegisterDate($date, $type = "=") {
        if($date)
		    $this->registerDate = $type."'$date'";
		return $this;
	}

    /**
     * @param $confirm
     * @return $this
     */
    function setConfirmed($confirm, $type = "=") {
        if(is_null($confirm) === false)
		    $this->confirmed = $type."'$confirm'";
		return $this;
	}

    /**
     * @param $role
     * @return Admin|Anonymous|User
     */
    function createByRole($role) {
		if($role == 'ROLE_USER') {
			$user = new User();
		}
		else if($role == 'ROLE_ADMIN'){
			$user = new Admin();
		}
		else {
			return new Anonymous();
		}
		return $user;
	}

    /**
     * @param $code
     * @return $this
     */
    function setPasswordResetCode($code, $type = "=") {
        if(is_null($code) === false)
		    $this->password_reset_code = $type."'$code'";
		return $this;
	}

    /**
     * @param $token
     * @return $this
     */
    function setSSID($token, $type = "=") {
        if(is_null($token) === false)
		    $this->ssid = $type."'$token'";
		return $this;
	}

    /**
     * @param $active
     * @return $this
     */
    function setActive($active, $type = "=") {
        if($active)
		    $this->active = $type."'$active'";
		return $this;
	}

    /**
     * @param $login
     * @return $this
     */
    function setLogin($login, $type = "=") {
        if(is_null($login) === false)
		    $this->login = $type."'$login'";
		return $this;
	}

    /**
     * @param $key
     * @return $this
     */
    function setConfirmKey($key, $type = "=") {
        if(is_null($key) === false)
		    $this->confirmKey = $type."'$key'";
		return $this;
	}

    /**
     * @param $password
     * @return $this
     */
    function setPassword($password, $type = "=") {
        if(is_null($password) === false)
		    $this->password = $type."'$password'";
		return $this;
	}

    /**
     *
     */
    function create() {
		$db = new PDODatabase();
		$db = $db->get();
		$query = $this->buildCreateQuery();
		$db->query($query);
	}

    /**
     * @return Admin|Anonymous|User
     */
    function get() {

		$db = new PDOdatabase();
		$db = $db->get();

		$query = $this->buildSelectQuery();
		$result = $db->query($query);
        if(!$result)
        {
            return new Anonymous();
        }
		if(!$m = $result->fetch()){
			return new Anonymous();
		}

		$user = $this->createByRole($m['role']);

		$user->setId($m['id']);
		$user->setSSID($m['ssid']);
		$user->setLogin($m['login']);
		$user->setRole($m['role']);
		$user->setConfirmed($m['confirmed']);
		$user->setConfirmKey($m['confirmKey']);
		$user->setActive($m['active']);
		$user->setLastLoginDate($m['lastLoginDate']);
		$user->setRegisterDate($m['registerDate']);
		$user->setLastActionDate($m['lastActionDate']);
		$user->setPasswordResetCode($m['passwordResetCode']);
        $user->setCanonicalLastActionDate($m['canonicalLastActionDate']);
        $user->setCanonicalLastLoginDate($m['canonicalLastLoginDate']);
        $user->setCanonicalRegisterDate($m['canonicalRegisterDate']);
        $user->setGender($m['gender']);
        $user->setUuid($m['uuid']);

        $this->setId($user->getId());

		return $user;

	}

	function getAll() {
		$collection = new UserCollection();

		$db = new PDOdatabase();
		$db = $db->get();

		$query = $this->buildSelectQuery();

		$result = $db->query($query);

		while($m = $result->fetch()){

			$user = $this->createByRole($m['role']);

			$user->setId($m['id']);
			$user->setSSID($m['ssid']);
			$user->setLogin($m['login']);
			$user->setConfirmed($m['confirmed']);
			$user->setConfirmKey($m['confirmKey']);
			$user->setActive($m['active']);
			$user->setLastLoginDate($m['lastLoginDate']);
			$user->setRegisterDate($m['registerDate']);
            $user->setLastActionDate($m['lastActionDate']);
			$user->setPasswordResetCode($m['passwordResetCode']);
			$user->setCanonicalLastActionDate($m['canonicalLastActionDate']);
			$user->setCanonicalLastLoginDate($m['canonicalLastLoginDate']);
			$user->setCanonicalRegisterDate($m['canonicalRegisterDate']);
            $user->setGender($m['gender']);
			$user->setIsBot($m['isBot']);
			$user->setUuid($m['uuid']);


			$collection->add($user);

		}

		return $collection;		
	}	

	function update() {
		$db = new PDOdatabase();
		$db = $db->get();
		$query = $this->buildUpdateQuery();
        echo $query;
		$db->query($query);
	}

	function delete() {
		$db = new PDOdatabase();
		$db = $db->get();

		$query = $this->buildDeleteQuery();

		$db->query($query);
	}

    /**
     * @param mixed $uuid
     * @return UserFactory
     */
    public function setUuid($uuid, $type = "=")
    {
        if($uuid)
            $this->uuid = $type."'$uuid'";
        return $this;
    }

    /**
     * @param mixed $lastActionDate
     * @return UserFactory
     */
    public function setLastActionDate($lastActionDate, $type = "=")
    {
        if(is_null($lastActionDate) === false)
            $this->lastActionDate = $type."'$lastActionDate'";
        return $this;
    }

    /**
     * @param mixed $isBot
     * @return UserFactory
     */
    public function setIsBot($isBot, $type = "=")
    {
        if(is_null($isBot) === false)
            $this->isBot = $type."'$isBot'";
        return $this;
    }

    /**
     * @param mixed $canonicalLastActionDate
     * @return UserFactory
     */
    public function setCanonicalLastActionDate($canonicalLastActionDate, $type = "=")
    {
        if(is_null($canonicalLastActionDate) === false)
            $this->canonicalLastActionDate = $type."'$canonicalLastActionDate'";
        return $this;
    }
    public function setCanonicalLastLoginDate($canonicalLastLoginDate, $type = "=")
    {
        if(is_null($canonicalLastLoginDate) === false)
            $this->canonicalLastLoginDate = $type."'$canonicalLastLoginDate'";
        return $this;
    }
    public function setCanonicalRegisterDate($canonicalRegisterDate, $type = "=")
    {
        if(is_null($canonicalRegisterDate) === false)
            $this->canonicalRegisterDate = $type."'$canonicalRegisterDate'";
        return $this;
    }

    /**
     * @param mixed $gender
     * @return UserFactory
     */
    public function setGender($gender, $type = "=")
    {
        if(is_null($gender) === false)
            $this->gender = $type."'$gender'";
        return $this;
    }
}