<?php
use PHPMailer\PHPMailer\PHPMailer;

class Mailer {
	private $mailer;
	function __construct() {
		$this->mailer = new PHPMailer(true);
		try {
		    //Server settings
			$router = new Router();
			$config = file_get_contents($router->getPath().'/server/Config/mailer.json');
			$config = json_decode($config);

		    $this->mailer->SMTPDebug = 1;                                 // Enable verbose debug output
		    $this->mailer->isSMTP();                                      // Set mailer to use SMTP
		    $this->mailer->Host = 'smtp.gmail.com';  					  // Specify main and backup SMTP servers
		    $this->mailer->SMTPAuth = true;                               // Enable SMTP authentication
		    $this->mailer->Username = $config->{"username"};              // SMTP username
		    $this->mailer->Password = $config->{"password"};              // SMTP password
		    $this->mailer->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $this->mailer->Port = 587;                                    // TCP port to connect to

	    	$this->mailer->setFrom($config->{"username"}, 'ZSTiE');
			
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	function addContext($subject, $context) {
		    //Content
		    $this->mailer->isHTML(true);                    				// Set email format to HTML
		    $this->mailer->Subject = $subject;
		    $this->mailer->Body    = $context;
		    return $this;
	}

	function addTarget($target, $name) {
		    //Recipients
		    $this->mailer->addAddress($target, $name);     // Add a recipient
		    return $this;
	}

	function send() {
		try {
		    $this->mailer->send();			
		} catch (Exception $e) {
			echo $e->getMessage();			
		}
	}
}