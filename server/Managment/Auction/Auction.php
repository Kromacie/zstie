<?php

class Auction {
    protected $id;
    protected $productId;
    protected $bidsCost;
    protected $lastUser;
    protected $endDate;
    protected $startDate;
    protected $finished;
    protected $currentBids;
    protected $canonicalEndDate;
    protected $canonicalStartDate;

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param mixed $productId
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @return mixed
     */
    public function getBidsCost()
    {
        return $this->bidsCost;
    }

    /**
     * @param mixed $bidsCost
     */
    public function setBidsCost($bidsCost)
    {
        $this->bidsCost = $bidsCost;
    }

    /**
     * @return mixed
     */
    public function getLastUser()
    {
        return $this->lastUser;
    }

    /**
     * @param mixed $lastUser
     */
    public function setLastUser($lastUser)
    {
        $this->lastUser = $lastUser;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param mixed $endDate
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * @param mixed $finished
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCurrentBids()
    {
        return $this->currentBids;
    }

    /**
     * @param mixed $currentBids
     */
    public function setCurrentBids($currentBids)
    {
        $this->currentBids = $currentBids;
    }

    /**
     * @return mixed
     */
    public function getCanonicalEndDate()
    {
        return $this->canonicalEndDate;
    }

    /**
     * @param mixed $canonicalEndDate
     */
    public function setCanonicalEndDate($canonicalEndDate)
    {
        $this->canonicalEndDate = $canonicalEndDate;
    }

    /**
     * @return mixed
     */
    public function getCanonicalStartDate()
    {
        return $this->canonicalStartDate;
    }

    /**
     * @param mixed $canonicalStartDate
     */
    public function setCanonicalStartDate($canonicalStartDate)
    {
        $this->canonicalStartDate = $canonicalStartDate;
    }

}