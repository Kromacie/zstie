<?php
class User extends \QueryBuilder\Entity\DBEntity{

    public $id;
    public $ssid;
    public $role;
    public $confirmKey;
    public $login;
    public $confirmed;
    public $active;
    public $registerDate;
    public $lastLoginDate;
    public $passwordResetCode;
    public $lastActionDate;
    public $uuid;
    public $isBot;
    public $canonicalLastActionDate;
    public $canonicalLastLoginDate;
    public $canonicalRegisterDate;
    public $gender;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $ssid
     */
    public function setSsid($ssid)
    {
        $this->ssid = $ssid;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @param mixed $confirmKey
     */
    public function setConfirmKey($confirmKey)
    {
        $this->confirmKey = $confirmKey;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @param mixed $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @param mixed $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @param mixed $lastLoginDate
     */
    public function setLastLoginDate($lastLoginDate)
    {
        $this->lastLoginDate = $lastLoginDate;
    }

    /**
     * @param mixed $passwordResetCode
     */
    public function setPasswordResetCode($passwordResetCode)
    {
        $this->passwordResetCode = $passwordResetCode;
    }

    /**
     * @param mixed $lastActionDate
     */
    public function setLastActionDate($lastActionDate)
    {
        $this->lastActionDate = $lastActionDate;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @param mixed $isBot
     */
    public function setIsBot($isBot)
    {
        $this->isBot = $isBot;
    }

    /**
     * @param mixed $canonicalLastActionDate
     */
    public function setCanonicalLastActionDate($canonicalLastActionDate)
    {
        $this->canonicalLastActionDate = $canonicalLastActionDate;
    }

    /**
     * @param mixed $canonicalLastLoginDate
     */
    public function setCanonicalLastLoginDate($canonicalLastLoginDate)
    {
        $this->canonicalLastLoginDate = $canonicalLastLoginDate;
    }

    /**
     * @param mixed $canonicalRegisterDate
     */
    public function setCanonicalRegisterDate($canonicalRegisterDate)
    {
        $this->canonicalRegisterDate = $canonicalRegisterDate;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    function __construct()
    {
        parent::__construct("users");
    }

    /**
     * @return mixed
     */
    public function getSsid()
    {
        return $this->ssid;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @return mixed
     */
    public function getConfirmKey()
    {
        return $this->confirmKey;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return mixed
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @return mixed
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetCode()
    {
        return $this->passwordResetCode;
    }

    /**
     * @return mixed
     */
    public function getLastActionDate()
    {
        return $this->lastActionDate;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return mixed
     */
    public function getisBot()
    {
        return $this->isBot;
    }

    /**
     * @return mixed
     */
    public function getCanonicalLastActionDate()
    {
        return $this->canonicalLastActionDate;
    }

    /**
     * @return mixed
     */
    public function getCanonicalLastLoginDate()
    {
        return $this->canonicalLastLoginDate;
    }

    /**
     * @return mixed
     */
    public function getCanonicalRegisterDate()
    {
        return $this->canonicalRegisterDate;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}