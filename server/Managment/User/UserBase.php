<?php

abstract class UserBase {

	protected $id;
	protected $ssid;
	protected $role;
	protected $confirmKey;
	protected $login;
	protected $confirmed;
	protected $active;
	protected $registerDate;
	protected $lastLoginDate;
	protected $passwordResetCode;
	protected $lastActionDate;
	protected $uuid;
	protected $isBot;
    protected $canonicalLastActionDate;
    protected $canonicalLastLoginDate;
    protected $canonicalRegisterDate;
    protected $gender;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSsid()
    {
        return $this->ssid;
    }

    /**
     * @param mixed $ssid
     */
    public function setSsid($ssid)
    {
        $this->ssid = $ssid;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * @param mixed $bids
     */
    public function setBids($bids)
    {
        $this->bids = $bids;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

    /**
     * @param mixed $confirmed
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * @param mixed $registerDate
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;
    }

    /**
     * @return mixed
     */
    public function getPasswordResetCode()
    {
        return $this->passwordResetCode;
    }

    /**
     * @param mixed $passwordResetCode
     */
    public function setPasswordResetCode($passwordResetCode)
    {
        $this->passwordResetCode = $passwordResetCode;
    }

    /**
     * @return mixed
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * @param mixed $lastLoginDate
     */
    public function setLastLoginDate($lastLoginDate)
    {
        $this->lastLoginDate = $lastLoginDate;
    }

    /**
     * @return mixed
     */
    public function getConfirmKey()
    {
        return $this->confirmKey;
    }

    /**
     * @param mixed $confirmKey
     */
    public function setConfirmKey($confirmKey)
    {
        $this->confirmKey = $confirmKey;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getLastActionDate()
    {
        return $this->lastActionDate;
    }

    /**
     * @param mixed $lastActionDate
     */
    public function setLastActionDate($lastActionDate)
    {
        $this->lastActionDate = $lastActionDate;
    }

    /**
     * @return mixed
     */
    public function getIsBot()
    {
        return $this->isBot;
    }

    /**
     * @param mixed $isBot
     */
    public function setIsBot($isBot)
    {
        $this->isBot = $isBot;
    }

    /**
     * @return mixed
     */
    public function getCanonicalLastActionDate()
    {
        return $this->canonicalLastActionDate;
    }

    /**
     * @param mixed $canonicalLastActionDate
     */
    public function setCanonicalLastActionDate($canonicalLastActionDate)
    {
        $this->canonicalLastActionDate = $canonicalLastActionDate;
    }

    /**
     * @return mixed
     */
    public function getCanonicalLastLoginDate()
    {
        return $this->canonicalLastLoginDate;
    }

    /**
     * @param mixed $canonicalLastLoginDate
     */
    public function setCanonicalLastLoginDate($canonicalLastLoginDate)
    {
        $this->canonicalLastLoginDate = $canonicalLastLoginDate;
    }

    /**
     * @return mixed
     */
    public function getCanonicalRegisterDate()
    {
        return $this->canonicalRegisterDate;
    }

    /**
     * @param mixed $canonicalRegisterDate
     */
    public function setCanonicalRegisterDate($canonicalRegisterDate)
    {
        $this->canonicalRegisterDate = $canonicalRegisterDate;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }


}