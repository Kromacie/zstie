<?php
class Anonymous extends UserBase {
	function __construct() {
		$this->role = "ROLE_ANONYMOUS";
		$this->setLogin('Anon');
	}
}