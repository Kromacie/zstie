<?php

class Date {

    function toSeconds($time) {
        return floor(strtotime($time));
    }

    function toMinutes($time) {
        return floor(strtotime($time) / 60);
    }

    function toHours($time) {
        return floor(strtotime($time) / 3600);
    }

    function toDays($time) {
        return floor(strtotime($time) / 86400);
    }

    function getCurrent() {
        return date("Y-m-d H:i:s");
    }

    function secondsToModulo($sec, $modulo) {
        return $sec % $modulo;
    }

}