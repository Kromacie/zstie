<?php

class AccessControl {

	private $route;
	private $config;
	private $router;

    /**
     * @param Router $router
     * @return $this
     */
    function handle(Router $router) {
		$this->valid = true;
		$this->router = $router;
		$this->route = $router->getSelectedRoute();
		$this->loadConfig();
		$this->setValidUri();
		return $this;
	}
	function verified() {
		return $this->valid;
	}
	function setValidUri() {
		$data = Session::get(['id', 'role']);
		$id = is_null($data['id']) === true ? 0 : $data['id'];
        $role = is_null($data['role']) === true ? 0 : $data['role'];

		$user = new User();
        $user->select()
                ->column("*")
              ->where()
                ->column("id")->is($id)->and()
                ->column("role")->is($role);

        $user->get();
        if($user->getRole() === null)
        {
            $user->setRole("ROLE_ANONYMOUS");
        }
		if($user->getRole() != null &&
            ($user->getSsid() != session_id() ||
           $user->getId() != $data['id'] ||
           $user->getRole() != $data['role'])){
		    Session::delete(['id', 'role']);
		    session_regenerate_id();
		    $user = new Anonymous();
        }

        if($user->getRole() === null &&(
            is_null($data['id']) === false ||
            is_null($data['role']) === false
            )){
            print_r($data);
		    Notificationer::set('Zostałeś wylogowany z powodów bezpieczeństwa!', false, '/', 1);
            Session::delete(['id', 'role']);
        }

		$role = $user->getRole();
		if(!$this->validAccess($role))
		{
			$this->valid = false;
			$role = $this->getRoleByUri();
			$uri = $this->getDefaultUriByRole($role);
			$route = $this->router->get($uri);
			$this->router->selectRoute($route);
			if($uri[0] == '/')
			{
				$uri = substr($uri, 1);
			}
			$this->router->createRoute(LOCAL.$uri);
			return false;
		}
	}
	function loadConfig() {
		$json = file_get_contents($this->router->getPath().'/server/Config/access.json');
		$config = json_decode($json);
		$this->debugConfig($config->{'ROLE_ANONYMOUS'});
		$this->debugConfig($config->{'ROLE_USER'});
		$this->debugConfig($config->{'ROLE_ADMIN'});
		$this->config = $config;
	}

	function getDefaultUriByRole($role) {
		return $this->config->{$role}->{'__default'};
	}

	function checkRoles($needle, $roles) {
		foreach ($roles as $role) {
			if($role == $needle) {
				return true;
			}
		}
		return false;
	}

	function validAccess($role) {
		$uri = $this->getUri();
        foreach($this->config->{$role}->{'__path'} as $path){
            if($path == $uri)
                return true;
        }
		return false;
	}

	function getRoleByUri() {
		foreach ($this->config as $role_name => $role_value) {
			foreach($role_value->{'__path'} as $path){
				if($path == $this->getUri())
					return $role_name;
			}
		}

		return 'ROLE_ANONYMOUS';
	}

	function debugConfig($config) {
		try {
			if(!isset($config->{'__path'}) || !isset($config->{'__default'})){
				throw new Exception('AccessControl: Błąd w pliku konfiguracyjnym<br />');
			}
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		return $config;
	}

	function getUri() {
		if(isset($this->route))
		{
			return $this->route->getPath();
		}
	}
}