<?php
namespace QueryBuilder\Engine;


use QueryBuilder\Query\Statement;

class Transaction
{
    private $db;
    protected $statements = [];

    /**
     * Translation constructor.
     */
    function __construct()
    {
        if (!empty(DBConfig::$ENGINE)) {
            $this->db = DBConfig::$ENGINE;
        }
    }
    function add(Statement $statement)
    {
        $this->statements[] = $statement;
        return $this;
    }


    function commit()
    {
        $this->db->beginTransaction();
        foreach ($this->statements as $statement)
        {
            $this->db->exec($statement->render());
        }
        $this->db->commit();
    }
}