<?php
namespace QueryBuilder\Engine;


use QueryBuilder\Query\Statement;

class Database
{
    private $db;
    protected $statements = [];

    /**
     * Translation constructor.
     */
    function __construct()
    {
        if (!empty(DBConfig::$ENGINE)) {
            $this->db = DBConfig::$ENGINE;
        }
    }
    function query(Statement $statement)
    {
        $query = $statement->render();
        $result =  $this->db->query($query);
        return $result;
    }

}