<?php
namespace QueryBuilder\Engine;

class DBConfig
{
    public static $DATABASE;
    public static $LOGIN;
    public static $PASSWORD;
    public static $HOST;
    public static $ENGINE;
    public final static function runPDO()
    {
        self::$ENGINE = new \PDO("mysql:host=".self::$HOST.";dbname=".self::$DATABASE,self::$LOGIN, self::$PASSWORD);
        self::run();
    }
    private final static function run(){}
}