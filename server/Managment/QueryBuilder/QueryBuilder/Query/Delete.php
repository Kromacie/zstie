<?php
namespace QueryBuilder\Query;

use QueryBuilder\Query\Modules\Delete\From;
use QueryBuilder\Query\Modules\Delete\Selector;
use QueryBuilder\Query\Modules\Where;

class Delete extends Statement
{
    protected $variableManager;
    protected $conditionManager;
    protected $tableManager;
    protected $expressionManager;

    function __construct()
    {
        $this->variableManager = new Selector($this);
        $this->tableManager = new From($this);
        $this->conditionManager = new Where($this);
    }

    public function where()
    {
        return $this->conditionManager;
    }

    public function expression()
    {
        // TODO: Implement expression() method.
    }

    public function render()
    {
        $query = "DELETE";
        $query .= $this->tableManager->render();
        $query .= $this->conditionManager->render();
        return $query.";";
    }

    function delete()
    {
        return $this->variableManager;
    }

    public function table()
    {
        return $this->tableManager;
    }
}
