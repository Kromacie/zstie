<?php
namespace QueryBuilder\Query\Modules;

use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\ExpressionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\VariablesAccess;
use QueryBuilder\Query\Modules\Table\Prefix;
use QueryBuilder\Query\Statement;
/**
 *
 */
abstract class Table extends Handler implements VariablesAccess, ConditionsAccess, Renderable, ExpressionsAccess
{
    protected $select;
    protected $tables = [];
    protected $prefix;
    function __construct(Statement $select)
    {
        $this->prefix = new Prefix();
        $this->select = $select;
    }
    function set($table)
    {
        $this->tables[$table] = $table;
        return $this;
    }

    /**
     * @return Where
     */
    function where()
    {
        return $this->select->where();
    }

    /**
     * @return Expression
     */
    function expression()
    {
        return $this->select->expression();
    }

    function render()
    {
        $query = "";
        foreach($this->tables as $table)
        {
            $query .= " `$table`,";
        }
        if($query != "")
        {
            return $this->prefix->render().substr($query, 0, -1);
        }
        return "";

    }
}



?>
