<?php
namespace QueryBuilder\Query\Modules;

use QueryBuilder\Query\Statement;

class Handler
{
    protected $handler;

    /**
     * @return Statement
     */
    protected function getHandler()
    {
        return $this->handler;
    }
    protected function setHandler(Statement $statement)
    {
        $this->handler = $statement;
    }
}