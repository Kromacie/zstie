<?php
namespace QueryBuilder\Query\Modules;

use QueryBuilder\Access\InsertAccess;
use QueryBuilder\Query\Insert;
/**
 *
 */
class Into extends Table implements InsertAccess
{
    protected $select;
    function __construct(Insert $select)
    {
        parent::__construct($select);
        $this->select = $select;
        $this->prefix->setPrefix("INTO");
    }

    function insert()
    {
        $this->select->insert();
        // TODO: Implement insert() method.
    }
}



?>
