<?php
namespace QueryBuilder\Query\Modules\Update;

use QueryBuilder\Query\Statement;

class Table extends \QueryBuilder\Query\Modules\Table
{
    function __construct(Statement $select)
    {
        parent::__construct($select);
        $this->select = $select;
    }
}