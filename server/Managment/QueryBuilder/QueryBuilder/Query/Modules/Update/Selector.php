<?php
namespace QueryBuilder\Query\Modules\Update;


use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Query\Modules\Update\Selector\Value;
use QueryBuilder\Query\Update;

class Selector implements Renderable, TableAccess, ColumnAccess, ConditionsAccess
{
    protected $update;
    protected $values = [];
    function __construct(Update $update)
    {
        $this->update = $update;
    }
    function where()
    {
        return $this->update->where();
    }
    function column($column)
    {
        $val = new Value($this);
        $val->column($column);
        $this->values[] = $val;
        return $val;
    }
    function render()
    {
        $query = " SET";
        foreach ($this->values as $value)
        {
            $query .= " ".$value->render().",";
        }
        return substr($query, 0, -1);
    }


    public function table()
    {
        return $this->update->table();
    }
}
?>
