<?php
namespace QueryBuilder\Query\Modules\Delete;

use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Query\Delete;

/**
 *
 */
class Selector implements Renderable, TableAccess, ConditionsAccess
{
  protected $select;
  protected $operators = [];
  function __construct(Delete $select)
  {
    $this->select = $select;
  }
  function table()
  {
    return $this->select->table();
  }
  function where()
  {
    return $this->select->where();
  }
  function render()
  {
    return "DELETE ";
  }
}

 ?>
