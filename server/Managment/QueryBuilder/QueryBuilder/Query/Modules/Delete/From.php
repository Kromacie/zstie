<?php
namespace QueryBuilder\Query\Modules\Delete;

use QueryBuilder\Access\DeleteAccess;
use QueryBuilder\Query\Delete;
use QueryBuilder\Query\Modules\Table;
/**
 *
 */
class From extends Table implements DeleteAccess
{
    function __construct(Delete $select)
    {
        parent::__construct($select);
        $this->select = $select;
        $this->prefix->setPrefix("FROM");
    }

    function delete()
    {
        return $this->select->delete();
    }
}



 ?>
