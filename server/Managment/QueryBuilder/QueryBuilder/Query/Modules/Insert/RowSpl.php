<?php
namespace QueryBuilder\Query\Modules\Insert;


use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Query\Insert;
use QueryBuilder\Query\Modules\Insert\RowSpl\Row;

class RowSpl implements Renderable, ColumnAccess, TableAccess
{
    protected $insert;
    protected $rows = [];
    function __construct(Insert $insert)
    {
        $this->insert = $insert;
    }
    function column($column)
    {
        $ob = new Row($this);
        $ob->setRow($column);
        $this->rows[] = $ob;
        return $this->insert->variables();
    }
    function render()
    {
        $query = " (";
        foreach ($this->rows as $row)
        {
            /** @var Row $row */
            $query .= $row->get().",";
        }
        return substr($query, 0, -1).")";
    }

    public function table()
    {
        return $this->insert->table();
    }
}