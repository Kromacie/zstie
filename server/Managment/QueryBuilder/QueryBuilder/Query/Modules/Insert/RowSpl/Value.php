<?php
namespace QueryBuilder\Query\Modules\Insert\RowSpl;

use QueryBuilder\Query\Modules\Insert\ValueSpl;

class Value
{
    protected $values;
    protected $value;
    function __construct(ValueSpl $values)
    {
        $this->values = $values;
    }
    function set($value)
    {
        $this->value = $value;
    }
    function get()
    {
        return $this->value;
    }
}