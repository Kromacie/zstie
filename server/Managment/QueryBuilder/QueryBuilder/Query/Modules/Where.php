<?php
namespace QueryBuilder\Query\Modules;

use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\ExpressionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Query\Modules\Where\Condition;
use QueryBuilder\Query\Statement;

/**
 *
 */
class Where extends Handler implements ColumnAccess, Renderable, ExpressionsAccess
{
  protected $select;
  protected $rows = [];
  protected $combine;
  protected $lastRow;
  protected $buffer;

  function __construct(Statement $select)
  {
    $this->buffer = new Condition($this);
    $this->select = $select;
  }
  function column($column, $table = false)
  {
    $this->lastRow = $column;
    $condition = clone $this->buffer;
    $condition->setColumn($column);
    if($table)
    {
      $condition->setTable($table);
    }
    $this->rows[$column] = $condition;
    $this->buffer->clear();
    return $condition;
  }
  function combine()
  {

    $this->buffer->openBracket();
    return $this;
  }
  function endCombine()
  {
    $this->rows[$this->lastRow]->closeBracket();
    return $this;
  }
  function render()
  {
    $query = "";
    foreach ($this->rows as $row)
    {
      $query .= " ".$row->render();
    }
    if($query != "")
    {
      return " WHERE".$query;
    }
    return "";

  }
    public function expression()
    {
        return $this->select->expression();
    }
}



 ?>
