<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Access\ColumnAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Access\VariablesAccess;
use QueryBuilder\Query\Modules\Select\Selector;

abstract class Operator implements TableAccess, Renderable, VariablesAccess
{

  public $row;
  public $table;
  protected $select;

  abstract function __construct(Selector $select);

  function column($value)
  {
    $this->row = $value;
  }
  function set($table)
  {
    $this->table = $table;
  }
  function table()
  {
    return $this->select->table();
  }
  function render()
  {
    return isset($this->table) ? $this->table.".".$this->row : $this->row;
  }

}

 ?>
