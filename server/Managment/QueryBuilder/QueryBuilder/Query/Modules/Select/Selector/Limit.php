<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Query\Modules\Expression;

class Limit
{
    protected $expression;
    protected $from;
    protected $to;

    function __construct(Expression $expression)
    {
        $this->expression = $expression;
    }
    function set($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }
    function render()
    {
        if(!isset($this->from))
        {
            return "";
        }
        return " LIMIT ".$this->from.", ".$this->to;
    }

}