<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Query\Modules\Select\Selector;

/**
 *
 */
class Sum extends Operator
{

    function __construct(Selector $select)
    {
        $this->select = $select;
    }
    function render()
    {
        return isset($this->table) ? "SUM($this->table".".$this->row)" : "SUM($this->row)";
    }
}

?>
