<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Query\Modules\Select\Selector;


/**
 *
 */
class Count extends Operator
{

    function __construct(Selector $select)
    {
        $this->select = $select;
    }
    function render()
    {
        return isset($this->table) ? "COUNT($this->table".".$this->row)" : "COUNT($this->row)";
    }
}

?>
