<?php
namespace QueryBuilder\Query\Modules\Select\Selector\OrderBy;

use QueryBuilder\Access\Renderable;
use QueryBuilder\Query\Modules\Select\Selector\OrderBy;

class Order implements Renderable
{
    protected $table;
    protected $row;
    protected $type;
    protected $orderBy;
    function __construct(OrderBy $orderBy)
    {
        $this->orderBy = $orderBy;
    }
    function setRow($row)
    {
        $this->row = $row;
    }
    function setType($type)
    {
        $this->type = $type;
    }
    function setTable($table)
    {
        $this->table = $table;
    }
    function render()
    {
        $query = "";
        $query .= isset($this->table) ? $this->table.".".$this->row : $this->row;
        $query .= isset($this->type) ? " ".$this->type : "";
        return $query;
    }
}