<?php
namespace QueryBuilder\Query\Modules\Select\Selector;

use QueryBuilder\Query\Modules\Select\Selector;


/**
 *
 */
class Normal extends Operator
{
  function __construct(Selector $select)
  {
    $this->select = $select;
  }
  function render()
  {
    return isset($this->table) ? $this->table.".".$this->row : $this->row;
  }
}

 ?>
