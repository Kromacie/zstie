<?php
namespace QueryBuilder\Query\Modules\Select;

use QueryBuilder\Access\SelectAccess;
use QueryBuilder\Query\Modules\Table;
use QueryBuilder\Query\Select;
/**
 *
 */
class From extends Table implements SelectAccess
{
    function __construct(Select $select)
    {
        parent::__construct($select);
        $this->select = $select;
        $this->prefix->setPrefix("FROM");
    }

    function select()
    {
        return $this->select->select();
    }
}



 ?>
