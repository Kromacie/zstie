<?php
namespace QueryBuilder\Query\Modules\Where;

use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Query\Modules\Handler;
use QueryBuilder\Query\Modules\Where;

class Condition extends Handler implements Renderable, ConditionsAccess
{
  protected $where;
  protected $row;
  protected $operator;
  protected $value;
  protected $__logic;
  protected $openB = 0;
  protected $closeB = 0;
  protected $logic = "";

  function __construct(Where $where)
  {
    $this->where = $where;
    $this->__logic = new Logic($this);
  }
  function __clone()
  {
    $this->__logic = new Logic($this);
  }
  function openBracket()
  {
    $this->openB++;
  }
  function closeBracket()
  {
    $this->closeB++;
  }
  function setColumn($column)
  {
    $this->row = $column;
  }
  function where()
  {
    return $this->where;
  }
  protected function setValue($value)
  {
    $this->value = $value;
  }
  function setTable($table)
  {
    $this->table = $table;
  }
  protected function setOperator($op)
  {
    $this->operator = $op;
  }
  function is($value)
  {
    $this->setValue($value);
    $this->setOperator("=");
    return $this->__logic;
  }
  function isBiggerThan($value)
  {
    $this->setValue($value);
    $this->setOperator(">");
    return $this->__logic;

  }
  function isSmallerThan($value)
  {
    $this->setValue($value);
    $this->setOperator("<");
    return $this->__logic;
  }
  function isLike($value)
  {
    $this->setValue("%$value%");
    $this->setOperator(" LIKE ");
    return $this->__logic;
  }
  function setLogic($logic)
  {
    $this->logic = $logic;
  }
  function clear()
  {
    $this->openB = 0;
    $this->closeB = 0;
  }
  function render()
  {
    $query = isset($this->table) ? $this->table.".".$this->row.$this->operator."'$this->value'" : $this->row.$this->operator."'$this->value'";
    if($this->openB > 0)
    {
      $bracketsO = "";
      for($i = 0; $i < $this->openB; $this->openB--)
      {
        $bracketsO .= "(";
      }
      $query = $bracketsO.$query;
    }
    if($this->closeB > 0)
    {
        $bracketsC = "";
        for($i = 0; $i < $this->closeB; $this->closeB--)
        {
            $bracketsC .= ")";
        }
        $query = $query.$bracketsC;
    }
    if($this->logic != "")
    {
      return $query." ".$this->logic;
    }
    return $query;
  }
}
 ?>
