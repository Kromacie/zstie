<?php
namespace QueryBuilder\Query\Modules;


use QueryBuilder\Access\ConditionsAccess;
use QueryBuilder\Access\Renderable;
use QueryBuilder\Access\TableAccess;
use QueryBuilder\Query\Modules\Select\Selector\Limit;
use QueryBuilder\Query\Modules\Select\Selector\OrderBy;
use QueryBuilder\Query\Select;

class Expression implements Renderable, TableAccess, ConditionsAccess
{
    protected $order;
    protected $limit;
    protected $select;
    function __construct(Select $select)
    {
        $this->select = $select;
        $this->order = new OrderBy($this);
        $this->limit = new Limit($this);
    }
    function render()
    {
        $query = " ";
        $query .= $this->order->render();
        $query .= $this->limit->render();
        return $query;
    }
    function limit($from, $to)
    {
        $this->limit->set($from, $to);
        return $this;
    }
    function orderBy()
    {
        return $this->order;
    }

    public function where()
    {
        return $this->select->where();
    }

    public function table()
    {
        return $this->select->table();
    }

    public function select()
    {
        return $this->select->select();
    }
}