<?php
namespace QueryBuilder\Query;

use QueryBuilder\Access\SelectorAccess;
use QueryBuilder\Query\Modules\Insert\RowSpl;
use QueryBuilder\Query\Modules\Insert\ValueSpl;
use QueryBuilder\Query\Modules\Into;
use QueryBuilder\Query\Modules\Where;

class Insert extends Statement implements SelectorAccess
{
    protected $variableManager;
    protected $conditionManager;
    protected $tableManager;
    protected $expressionManager;
    protected $columnManager;

    function __construct()
    {
        $this->columnManager = new RowSpl($this);
        $this->variableManager = new ValueSpl($this);
        $this->tableManager = new Into($this);
        $this->conditionManager = new Where($this);
    }

    public function where()
    {
        // TODO: Implement where() method.
    }

    public function expression()
    {
        // TODO: Implement expression() method.
    }

    public function render()
    {
        $query = "INSERT";
        $query .= $this->tableManager->render();
        $query .= $this->columnManager->render();
        $query .= $this->variableManager->render();

        return $query.";";

    }

    function insert()
    {
        return $this->columnManager;
    }

    public function table()
    {
        return $this->tableManager;
    }

    function variables()
    {
        return $this->variableManager;
    }
}
