<?php
namespace QueryBuilder\Query;

use QueryBuilder\Query\Modules\Update\Selector;
use QueryBuilder\Query\Modules\Update\Table;
use QueryBuilder\Query\Modules\Where;

class Update extends Statement
{
    protected $variableManager;
    protected $conditionManager;
    protected $tableManager;
    protected $expressionManager;

    function __construct()
    {
        $this->variableManager = new Selector($this);
        $this->tableManager = new Table($this);
        $this->conditionManager = new Where($this);
    }

    public function where()
    {
        return $this->conditionManager;
    }

    public function expression()
    {
        // TODO: Implement expression() method.
    }

    public function render()
    {
        $query = "UPDATE";
        $query .= $this->tableManager->render();
        $query .= $this->variableManager->render();
        $query .= $this->conditionManager->render();
        return $query.";";
    }

    function update()
    {
        return $this->variableManager;
    }

    public function table()
    {
        return $this->tableManager;
    }
}
