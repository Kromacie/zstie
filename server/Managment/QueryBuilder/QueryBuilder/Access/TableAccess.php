<?php
namespace QueryBuilder\Access;

interface TableAccess
{
    public function table();
}