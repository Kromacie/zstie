<?php
namespace QueryBuilder\Access;

interface UpdateAccess
{
    function update();
}