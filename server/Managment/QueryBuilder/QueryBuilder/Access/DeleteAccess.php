<?php
namespace QueryBuilder\Access;

interface DeleteAccess
{
    function delete();
}