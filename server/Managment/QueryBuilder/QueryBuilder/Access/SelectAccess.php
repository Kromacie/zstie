<?php
namespace QueryBuilder\Access;

interface SelectAccess
{
    function select();
}