<?php
namespace QueryBuilder\Access;

interface ExpressionsAccess
{
    public function expression();
}