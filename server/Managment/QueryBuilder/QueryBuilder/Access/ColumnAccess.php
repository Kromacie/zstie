<?php
namespace QueryBuilder\Access;

interface ColumnAccess
{
    function column($name);
}