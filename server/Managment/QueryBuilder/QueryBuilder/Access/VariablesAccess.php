<?php
namespace QueryBuilder\Access;

interface VariablesAccess
{
    function set($variable);
}