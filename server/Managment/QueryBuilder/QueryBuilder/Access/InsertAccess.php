<?php
namespace QueryBuilder\Access;

interface InsertAccess
{
    function insert();
}