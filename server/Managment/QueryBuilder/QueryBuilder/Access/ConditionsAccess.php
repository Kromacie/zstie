<?php
namespace QueryBuilder\Access;

interface ConditionsAccess
{
    public function where();
}