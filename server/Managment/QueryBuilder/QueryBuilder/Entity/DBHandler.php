<?php
namespace QueryBuilder\Entity;

use QueryBuilder;

abstract class DBHandler
{
    private $factory;
    private $statement;
    private $table;
    private $handler;
    function __construct($table)
    {
        $this->factory = new QueryBuilder();
        $this->table = $table;
    }
    function handle(DBEntity $entity)
    {
        $this->handler = $entity;
        $this->table = $entity->table;
        return $this;
    }
    function getHandler()
    {
        return $this->handler;
    }
    function update()
    {
        $this->statement = $this->factory->update();
        $this->statement->table()
                        ->set($this->table);
        return $this->statement->update();
    }
    function delete()
    {
        $this->statement = $this->factory->delete();
        $this->statement->table()
                        ->set($this->table);
        return $this->statement->delete();
    }
    function insert()
    {
        $this->statement = $this->factory->insert();
        $this->statement->table()
                        ->set($this->table);
        return $this->statement->insert();
    }
    function select()
    {
        $this->statement = $this->factory->select();
        $this->statement->table()
                        ->set($this->table);
        return $this->statement->select();
    }
    function getStatement()
    {
        return $this->statement;
    }

}