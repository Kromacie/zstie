<?php
namespace QueryBuilder\Entity;

use QueryBuilder\Engine\Database;
use QueryBuilder\Query\Select;

abstract class DBEntity extends DBHandler
{
    public $__table;
    function __construct($table)
    {
        $this->__table = $table;
        parent::__construct($table);
    }
    function get()
    {
        $db = new Database();
        $result = $db->query($this->getStatement());
        if(!$result)
        {
            return;
        }
        if($this->getStatement() instanceof Select)
        {
            while ($m = $result->fetch())
            {
                foreach ($m as $key => $value)
                {
                    if(property_exists($this, $key))
                    {
                        $this->$key = $value;
                    }
                }
            }

        }

    }

    function __toString()
    {
        $arr = [];
        foreach ($this as $key => $value)
        {
            $arr[$key] = $value;
        }
        return json_encode($arr);
    }
}