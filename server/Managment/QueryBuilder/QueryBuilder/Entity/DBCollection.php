<?php
namespace QueryBuilder\Entity;

use QueryBuilder\Engine\Database;

abstract class DBCollection extends DBHandler
{
    public $items = [];
    function __construct()
    {
        parent::__construct("");
    }
    function get()
    {
        $db = new Database();
        $result = $db->query($this->getHandler()->getStatement());
        if(!$result)
        {
            return;
        }
        while($m = $result->fetch())
        {
            $item = clone $this->getHandler();
            foreach ($m as $key => $value)
            {
                if(property_exists($item, $key))
                {
                    $item->$key = $value;
                }
            }
            $this->items[] = $item;
        }

    }
    function __toString()
    {
        $items = array();
        foreach ($this->items as $item)
        {
            $items[] = $item->__toString();
        }
        return json_encode($items);
    }
}