<?php
class QueryBuilder
{
    function select()
    {
        return new \QueryBuilder\Query\Select();
    }
    function delete()
    {
        return new \QueryBuilder\Query\Delete();
    }
    function update()
    {
        return new QueryBuilder\Query\Update();
    }
    function insert()
    {
        return new QueryBuilder\Query\Insert();
    }
}