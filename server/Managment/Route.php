<?php
class Route {
	protected $path;
	protected $args;
	protected $__args;
	function __construct($path, $controller){
		$this->args = [];
		$this->path = $path;
		$this->controller = $controller;

	}
	function applyArgsPos($args)
	{
		$this->__args = $args;
	}
	function getPath()
	{
		return $this->path;
	}
	function getRole()
	{
		return $this->role;
	}
	function route()
	{
		$class = $this->controller[0]."Controller";
		$method = $this->controller[1]."Action";
		$args = $this->args;
		$con = new $class;
		return $con->$method($args);
	}
	function getVariablesFromUri($uri)
	{
		if($uri == null)
		{
			$uri = '/';
		}
		$subs = explode('/', $uri);
		foreach ($this->__args as $key => $value) {
			if(isset($subs[$key])){
				$this->args[$value] = $subs[$key]; 
			} else {
				$this->args[$value] = null;
			}
		}
	}
}