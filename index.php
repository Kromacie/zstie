<?php
session_start();
require_once ("server/define.php");
require_once ("server/bootstrap.php");
error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 'On');
define("MV", dirname(__FILE__));
$db = new PDODatabase();
$router = new Router();
$router->load();
$router->route();

