
<meta charset="utf-8">
	<title>ZSTiE Wrocław</title>
	<!-- <meta http-equiv="refresh" content="1"> -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:title" content="ZSTiE Wrocław">
	<meta property="og:site_name" content="ZSTiE">
	<meta property="og:type" content="website">
	<meta property="og:locate" content="pl_PL">
	<meta property="og:image" content="<?=$view?>img/logo.png">
	<meta property="og:description" content="Oficjalna strona zespółu szkół teleinformatycznych i elektronicznych we Wrocławiu!">
	<meta name="title" content="ZSTiE Wrocław">
	<meta name="description" content="Oficjalna strona zespółu szkół teleinformatycznych i elektronicznych we Wrocławiu!">
	<meta name="ROBOTS" content="noodp">
	<link rel="icon" type="image/png" href="<?=$view?>img/logo.png">
	<link rel="stylesheet" type="text/css" href="<?=$style?>css/reset.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?=$style?>css/main.css">
	<!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script><![endif]-->