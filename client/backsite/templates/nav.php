<div id="nav-wrapper">
	<div class="nav"><a href="<?=$client?>">ZSTiE</a></div>
	<div class="nav"><a href="<?=$client?>nowosci">Nowości</a></div>
	<div class="nav"><a href="#">Rekrutacja <i class="fa fa-angle-down fa-nav"></i></a></div>
	<div class="nav"><a href="#">Szkoła <i class="fa fa-angle-down fa-nav"></i></a>
		<div class="big-nav">
			<div class="big-nav-col">
				<p class="nav-p">o nas</p>
				<img src="<?=$view?>img/nav/school.jpg" alt="szkoła">
				<li><a href="#">Historia <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Nasi patroni <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Galeria <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Kontakt <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col">
				<p class="nav-p">kierunki</p>
				<img src="<?=$view?>img/nav/directions.jpg" alt="kierunki">
				<li><a href="#">Technik informatyk <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Technik elektronik <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Elektronik <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col">
				<p class="nav-p">kadra</p>
				<img src="<?=$view?>img/nav/personnel.jpg" alt="kadra pedagogiczna">
				<li><a href="#">Kadra pedagogiczna <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Pedagodzy <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Rada rodziców <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Samorząd uczniowski <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col">
				<p class="nav-p">biblioteka</p>
				<img src="<?=$view?>img/nav/library.jpg" alt="biblioteka">
				<li><a href="#">O bibliotece <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Blog <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Katalog <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
		</div>
	</div>
	<div class="nav"><a href="#">Uczniowie <i class="fa fa-angle-down fa-nav"></i></a>
		<div class="big-nav">
			<div class="big-nav-col">
				<p class="nav-p">Organizacja pracy</p>
				<img src="<?=$view?>img/nav/calendar.jpg" alt="organizacja pracy">
				<li><a href="#">Plan lekcji <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Zastępstwa <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Kalendarz roku 17/18 <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Podręczniki <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col">
				<p class="nav-p">Egzaminy</p>
				<img src="<?=$view?>img/nav/exams.jpg" alt="egzaminy">
				<li><a href="#">Egzamin maturalny <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Egzamin zawodowy <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Wyniki egzaminów <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col">
				<p class="nav-p">Dodatkowe</p>
				<img src="<?=$view?>img/nav/optional.jpg" alt="kadra pedagogiczna">
				<li><a href="#">Kalendarz imprez <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Zajęcia pozalekcyjne <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Projekty <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col">
				<p class="nav-p">Informacje prawne</p>
				<img src="<?=$view?>img/nav/law.jpg" alt="szkoła">
				<li><a href="#">Akty prawne <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Przetargi <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
		</div>
	</div>
	<div class="nav"><a href="#">Wydarzenia <i class="fa fa-angle-down fa-nav"></i></a></div>
	<div class="nav"><a href="#">Galeria</a></div>
	<div class="nav"><a href="#">Języki <i class="fa fa-angle-down fa-nav"></i></a>
		<div class="big-nav">
			<div class="big-nav-col col-1-2 flp">
				<p class="nav-p">Angielski</p>
				<img src="<?=$view?>img/nav/english.jpg" alt="angielski">
				<li><a href="#">Poradniki <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Konkursy <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
			<div class="big-nav-col col-1-2 flp">
				<p class="nav-p">Niemiecki</p>
				<img src="<?=$view?>img/nav/german.jpg" alt="niemiecki">
				<li><a href="#">Poradniki <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
				<li><a href="#">Konkursy <i class="fa fa-angle-right fa-sub-nav"></i></a></li>
			</div>
		</div>
	</div>
	<div class="nav"><a href="#">Kontakt</a></div>
</div>