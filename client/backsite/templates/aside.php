<div id="aside-wrapper">
	<div id="tfb_slidesA">
		<div id="tfb_left">&#10094;</div>
		<div id="tfb_right">&#10095;</div>
		<div id="tfb_dots"></div>
		<div id="tfb_photos">
			<div class="tfb_photo">
				<img src="<?=$view?>img/slides/1.jpg" alt="slide-1">
				<p class="tfb_slides_p"><a href="#1">Lorem ipsum dolor sit amet.</a></p>
			</div>
			<div class="tfb_photo">
				<img src="<?=$view?>img/slides/2.jpg" alt="slide-2">
				<p class="tfb_slides_p"><a href="#2">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, ipsa.</a></p>
			</div>
			<div class="tfb_photo">
				<img src="<?=$view?>img/slides/3.jpg" alt="slide-3"></a>
				<p class="tfb_slides_p"><a href="#3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque quod enim est deserunt, eveniet in!</a></p>
			</div>
			<div class="tfb_photo">
				<img src="<?=$view?>img/slides/4.jpg" alt="slide-4">
				<p class="tfb_slides_p"><a href="#4">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ratione natus assumenda nam cum quaerat asperiores inventore repellendus quod omnis!</a></p>
			</div>
			<div class="tfb_photo">
				<img src="<?=$view?>img/slides/5.jpg" alt="slide-5">
				<p class="tfb_slides_p"><a href="#5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad debitis ullam facere suscipit tenetur, voluptates nostrum. Officiis, tenetur, eos. Natus dolorum a dicta accusamus maiores?</a></p>
			</div>
		</div>
	</div>
	<div id="collabs">
		<p class="site-main-p">nasi partnerzy</p>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/wsb.png" alt="wyższa szkoła bankowa"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/ibm.png" alt="ibm"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/samorzad.jpg" alt="samorząd"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/pryzmat_b.jpg" alt="samorząd"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/doradztwo.jpg" alt="samorząd"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/akademia_cisco2.png" alt="samorząd"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/robotyczny.png" alt="samorząd"></a>
		</div>
		<div class="collab">
			<a href="#"><img src="<?=$view?>img/socials/bip_logo_2.jpg" alt="samorząd"></a>
		</div>
		<div class="reset"></div>
	</div>
</div>