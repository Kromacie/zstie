<div id="header-wrapper">
	<div id="header-header">
		<a href="<?=$client?>" class="header-object"><img src="<?=$view?>img/logo.png" alt="logo ZSTiE"></a>
		<a href="<?=$client?>" class="header-object"><h2 class="header-twin-header">ZSTiE we Wrocławiu</h2><h4 class="header-twin-header">Zespół Szkół Teleinformatycznych i Elektronicznych</h4></a>
	</div>
	<div id="header-user">
        <?php
        if($is_granted("ROLE_USER") || $is_granted("ROLE_ADMIN")) {
            ?>
            <a href="<?= $client ?>profil">Twój profil</a>
            <?php
            if($is_granted('ROLE_ADMIN')){ 
            ?>
                <a href="<?= $client ?>zarejestruj">Zarejestruj</a>
            <?php
            }
            ?>
            <a href="<?= $client ?>wyloguj">Wyloguj</a>
            <?php
        } else {
            ?>
            <a href="<?= $client ?>zaloguj">Zaloguj</a>
            <?php
        }
        ?>
	</div>
</div>