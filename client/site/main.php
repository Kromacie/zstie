<!DOCTYPE html>
<html>
<head>
	<?=$load($head)?>
</head>
<body>

<div id="wrapper">
    <?=$notify?>
	<header>
        <?=$load($header)?>
	</header>

	<nav>
        <?=$load($nav)?>
	</nav>

	<aside>
		<?=$load($aside)?>
	</aside>

	<main>
		<div id="content-wrapper">
			<div id="news-wrapper">
				<?php
					if($is_granted('ROLE_USER') || $is_granted('ROLE_ADMIN')){
					?>
						<div id="add-news"><a href="<?=$client?>nowosci/dodaj">Dodaj artykuł</a></div>
					<?php
					}
				?>
				<div class="news">
					<article>
						<div class="news-title"><a href="#"><header>Tytuł i link</header></a></div>
						<div class="news-about">
							<section><div class="news-author">Autor artykułu</div></section>
							<section><div class="news-date">DD-MM-RRRR</div></section>
						</div>
						<section><div class="news-img"><img src="<?=$view?>img/slides/1.jpg" alt="slide-1"></div></section>
						<section><div class="news-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus dolorum fugiat eligendi, amet perspiciatis fugit consequatur accusantium modi voluptas, architecto explicabo facilis laboriosam laborum, deleniti ad nam est eaque omnis?</div></section>
					</article>
				</div>
				<div class="news">
					<article>
						<div class="news-title"><a href="#"><header>Tytuł i link</header></a></div>
						<div class="news-about">
							<section><div class="news-author">Autor artykułu</div></section>
							<section><div class="news-date">DD-MM-RRRR</div></section>
						</div>
						<section><div class="news-img"><img src="<?=$view?>img/slides/2.jpg" alt="slide-1"></div></section>
						<section><div class="news-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus dolorum fugiat eligendi, amet perspiciatis fugit consequatur accusantium modi voluptas, architecto explicabo facilis laboriosam laborum, deleniti ad nam est eaque omnis?</div></section>
					</article>
				</div>
			</div>
		</div>
	</main>

	<footer>
		<?=$load($footer)?>
	</footer>
</div>

	<script src="<?=$style?>js/jquery.min.js"></script>
	<script src="<?=$style?>js/mobile_menu.js"></script>
	<script src="<?=$style?>js/slides_standardA.js"></script>

</body>
</html>