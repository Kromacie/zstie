<!DOCTYPE html>
<html>
<head>
	<?=$load($head)?>
	<script src="<?=$style?>js/jquery.min.js"></script>
	<script src="<?=$style?>js/mobile_menu.js"></script>
</head>
<body>

<div id="wrapper">
	<header>
        <?=$load($header)?>
	</header>

	<nav>
        <?=$load($nav)?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div id="add-news-form">
				<form action="<?=$path?>" method="post">
					<input type="text" name="news_title" placeholder="Wpisz tytuł artykułu..." required>
					<span>Tytuł może zawierać od X do XX znaków.</span>
					
					<input type="file" name="news_image" required>
					<span>Dodaj obrazek prezentujący artykuł.</span>

					<textarea name="news_description"></textarea>
					<input type="submit" name="news_adding" value="Dodaj artykuł">
				</form>
			</div>
		</div>
	</main>

	<footer>
        <?=$load($footer)?>
	</footer>
</div>



</body>
</html>