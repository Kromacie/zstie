<!DOCTYPE html>
<html>
<head>
	<?php $load($head); ?>
</head>
<body>

<div id="wrapper">
    <?=$notify?>
	<header>
		<?php $load($header); ?>
	</header>

	<nav>
		<?php $load($nav); ?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div class="form-content">
				<form action="<?=$path?>" method="post">
					<input type="text" name="new_email" placeholder="Podaj nowy email..." required>
					<input type="submit" name="user_email_change" value="Zmień email">
                    <input type="hidden" name="token" value="<?=$token?>">
				</form>
			</div>
		</div>
	</main>

	<footer>
		<?php $load($footer); ?>
	</footer>
</div>

	<script src="<?=$style?>js/jquery.min.js"></script>
	<script src="<?=$style?>js/mobile_menu.js"></script>

</body>
</html>