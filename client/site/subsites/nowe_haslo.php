<!DOCTYPE html>
<html>
<head>
	<?php $load($head); ?>
</head>
<body>

<div id="wrapper">
    <?=$notify?>
	<header>
		<?php $load($header); ?>
	</header>

	<nav>
		<?php $load($nav); ?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div class="form-content">
				<form action="<?=$path?>" method="post">
					<input type="password" name="new_password1" placeholder="Podaj nowe hasło..." required>
					<input type="password" name="new_password2" placeholder="Powtórz nowe hasło..." required>
                    <input type="hidden" name="token" value="<?=$token?>">
					<input type="submit" name="user_pass_change" value="Zmień hasło">
				</form>
			</div>
		</div>
	</main>

	<footer>
		<?php $load($footer); ?>
	</footer>
</div>

	<script src="<?=$style?>js/jquery.min.js"></script>
	<script src="<?=$style?>js/mobile_menu.js"></script>

</body>
</html>