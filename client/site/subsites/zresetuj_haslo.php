<!DOCTYPE html>
<html>
<head>
	<?php $load($head); ?>
</head>
<body>

<div id="wrapper">
	<?=$notify?>
	<header>
		<?php $load($header); ?>
	</header>

	<nav>
		<?php $load($nav); ?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div class="form-content">
				<form action="<?=$path?>" method="post">
					<p class="forms-special-1">Podaj nam swój adres email, a my przyślemy wiadomość w celu zmiany hasła.</p>
					<input type="text" name="user_email" placeholder="Podaj adres email..." required>
					<input type="submit" name="user_reset_password" value="Wyślij wiadomość">
					<input type="hidden" name="token" value="<?=$token?>">
				</form>
			</div>
		</div>
	</main>

	<footer>
		<?php $load($footer); ?>
	</footer>
</div>

<!--   -->

</body>
</html>