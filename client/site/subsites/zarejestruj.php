<!DOCTYPE html>
<html>
<head>
	<?=$load($head)?>
</head>
<body>

<div id="wrapper">
	<?=$notify?>
	<header>
        <?=$load($header)?>
	</header>

	<nav>
        <?=$load($nav)?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div class="form-content">
				<form action="<?=$path?>" method="post">
                    <input type="text" name="user_email" placeholder="Podaj email..." required>
					<span>Login powinien składać się od 6 do 14 znaków i może zawierać wyłącznie znaki alfanumeryczne(bez polskich ogonków)</span>

					<input type="password" name="user_password1" placeholder="Podaj hasło..." required>
					<span>Hasło musi składać się od 8 do 16 znaków i może zawierać wyłącznie znaki alfanumeryczne(bez polskich ogonków)</span>
					<input type="password" name="user_password2" placeholder="Powtórz hasło..." required>

					<span>Wybierz swoją płeć:</span><br>
					<input type="radio" name="user_gender" value="1" checked required><span>Mężczyzna</span><br>
					<input type="radio" name="user_gender" value="0" required><span>Kobieta</span><br>

                    <input type="hidden" name="token" value="<?=$token?>">

					<span>Akceptuje <a href="#">regulamin</a> </span><input type="checkbox" name="website_privacy" required>
					
					<input type="submit" name="user_adding" value="Zarejestruj konto">
				</form>
			</div>
		</div>
	</main>

	<footer>
        <?=$load($footer)?>
	</footer>
</div>

	<script src="<?=$style?>js/jquery.min.js"></script>
	<script src="<?=$style?>js/mobile_menu.js"></script>
	<script src="<?=$style?>js/slides_standardA.js"></script>

</body>
</html>