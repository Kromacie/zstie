<!DOCTYPE html>
<html>
<head>
	<?=$load($head)?>
</head>
<body>
<div id="wrapper">
    <?=$notify?>
	<header>
		<?=$load($header)?>
	</header>

	<nav>
		<?=$load($nav)?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div class="form-content">
				<form action="<?=$path?>" method="post">
					<input type="text" name="login" placeholder="Podaj swój login lub email..." required>
					<input type="password" name="password" placeholder="Podaj hasło..." required>
                    <input type="hidden" name="token" value="<?=$token?>">
					<input type="submit" name="user_log" value="Zaloguj">
				</form>
				<a href="<?=$client?>zresetuj_haslo">Nie pamiętasz hasła?</a>
			</div>
		</div>
	</main>

	<footer>
		<?=$load($footer)?>
	</footer>
</div>

	<script src="<?=$style?>js/jquery.min.js"></script>
	<script src="<?=$style?>js/mobile_menu.js"></script>

</body>
</html>