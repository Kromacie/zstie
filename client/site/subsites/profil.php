<!DOCTYPE html>
<html>
<head>
	<?php $load($head); ?>
</head>
<body>

<div id="wrapper">
    <?=$notify?>
	<header>
		<?php $load($header); ?>
	</header>

	<nav>
		<?php $load($nav); ?>
	</nav>

	<main>
		<div id="content-wrapper">
			<div id="profile-details">
				<p class="site-main-p">Twoje dane</p>
				<table>
					<tr>
						<th>Nazwa użytkownika:</th>
						<td><span><?=$name?></span></td>
						<td><a href="<?=$client?>profil/zmien_email">Zmień</a></td>
					</tr>
					<tr>
						<th>Hasło użytkownika:</th>
						<td><span>************</span></td>
						<td><a href="<?=$client?>profil/zmien_haslo">Zmień</a></td>
					</tr>
				</table>
			</div>
			<!-- <div id="profile-changes">
				<p class="site-main-p">Zmień dane <span>(minimum jedno)<span></p>
				<div class="form-content profile-form">
					<form action="#" method="get">
						<input type="text" name="login" placeholder="Zmień nazwę użytkownika">
						<input type="text" name="email" placeholder="Zmień email użytkownika">
						<input type="password" name="password" placeholder="Podaj stare hasło">
						<input type="password" name="password" placeholder="Podaj nowe hasło">
						<input type="password" name="password" placeholder="Powtórz nowe hasło">
						<input type="submit" name="changes_adding" value="Zatwierdź zmiany">
					</form>
				</div>
			</div> -->
		</div>
	</main>

	<footer>
		<?php $load($footer); ?>
	</footer>
</div>

	<script src="<?=SF?>js/jquery.min.js"></script>
	<script src="<?=SF?>js/mobile_menu.js"></script>

</body>
</html>