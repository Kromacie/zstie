/**
*DOCUMENT-SCRIPT BY DANIEL GRODZICKI
*COPYRIGHT BY ZSTiE.pl // all rights reserved
*LAST-MODIFIED: 2017.09.04
*/
$(document).ready(function()
{
	$("#RWD_MENU > i").click(function()
	{
		$("#nav-wrapper").slideToggle(250);
	})

	$(".navbar > i.RE").click(function()
	{
		$(".REnb").slideToggle(250);
	})

	$(".navbar > i.SZ").click(function()
	{
	$(".SZnb").slideToggle(250);
	})

	$(".navbar > i.UCZ").click(function()
	{
	$(".UCZnb").slideToggle(250);
	})
})