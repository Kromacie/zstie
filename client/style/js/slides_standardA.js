var picturesBeltX = $('#tfb_slidesA > #tfb_photos');
var picturesA = $('#tfb_slidesA > #tfb_photos > .tfb_photo > img');
var leftArrowA = $('#tfb_slidesA > #tfb_left');
var rightArrowA = $('#tfb_slidesA > #tfb_right');
var countA = 0;
$(function()
{
	appendDotsA();
	slidesA();
	setInterval(dimensions, 0);
	autoA = setTimeout(autoSliderA, 8000);

	function dimensions()
	{
		picturesA.innerHeight((window.innerWidth-200));
	}

	function appendDotsA()
	{
		var dotsABox = $('#tfb_slidesA > #tfb_dots');

		for(i = 0; i<picturesA.length; i++)
		{
			dotsABox.append("<span class='dot'></span>");
		};

		this.dotsA = $('#tfb_slidesA > #tfb_dots > .dot');
		dotsA.eq(0).css("background", "#ccc");
	};

	function slidesA()
	{
		if(countA < 0)
		{
			countA = picturesA.length-1;
		}
		else if(countA > picturesA.length-1)
		{
			countA = 0;
		}
			picturesBeltX.css("transform", "translateX(-"+(countA*100)+"%)");
			dotsA.css("background", "none");
			dotsA.eq(countA).css("background", "#ccc");
	};

	function autoSliderA()
	{
		countA += 1;
		slidesA();
		autoA = setTimeout(autoSliderA, 8000);
	};

	//buttons
	picturesA.on('click', function()
	{
		clearTimeout(autoA);
		autoSliderA();
	});
	leftArrowA.on('click', function()
	{
		countA -= 2;
		clearTimeout(autoA);
		autoSliderA();
	});
	rightArrowA.on('click', function()
	{
		clearTimeout(autoA);
		autoSliderA();
	});
	dotsA.on('click', function()
	{
		clearTimeout(autoA);
		countA = dotsA.index(this)-1;
		autoSliderA();
	});
});